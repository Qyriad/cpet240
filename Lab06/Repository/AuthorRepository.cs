﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using Qyriad.Lab06.DataModel;

namespace Qyriad.Lab06.Repository
{
	public interface IRepository<T>
	{
		List<T> FindAll();
	}

	public class AuthorRepository : IRepository<Author>
	{
		public struct Field
		{
			public string Header;
		}

		public List<Author> Authors { get; private set; }
		public List<Field> Fields { get; private set; }

		private XmlNode RootNode;


		public AuthorRepository(XmlNode rootNode)
		{
			this.RootNode = rootNode;
		}

		public AuthorRepository(List<List<object>> sqlData, IEnumerable<string> cols)
		{
			this.Authors = new List<Author>();
			this.Fields = new List<Field>();
			foreach(string col in cols)
			{
				Fields.Add(new Field { Header = col });
			}

			foreach(List<object> fields in sqlData)
			{
				string id = "";
				string lname = "";
				string fname = "";
				string phone = "";
				string stAddr = "";
				string city = "";
				string state = "";
				string zip = "";
				bool contract = false;

				for(int i = 0; i < fields.Count; i++)
				{
					switch(i)
					{
						case 0: // au_id
							id = fields[i].ToString();
							break;
						case 1: // au_lname
							lname = fields[i].ToString();
							break;
						case 2:
							fname = fields[i].ToString();
							break;
						case 3:
							phone = fields[i].ToString();
							break;
						case 4:
							stAddr = fields[i].ToString();
							break;
						case 5:
							city = fields[i].ToString();
							break;
						case 6:
							state = fields[i].ToString();
							break;
						case 7:
							zip = fields[i].ToString();
							break;
						case 8:
							contract = (bool)fields[i];
							break;
					}
				}
				Author a = new Author(id, lname, fname, phone, stAddr, city, state, zip, contract);
				this.Authors.Add(a);
			}
		}

		public List<Author> FindAll()
		{
			//this.Authors = new List<Author>();
			//this.Fields = new List<Field>();
			//Field first = new Field();
			//first.Header = this.RootNode.FirstChild.Attributes[0].Name;

			//foreach(XmlNode fieldNode in this.RootNode.FirstChild)
			//{
			//	Field f = new Field();
			//	if(fieldNode.ChildNodes.Count > 1) // Account for contact nodes
			//	{
			//		foreach(XmlNode ci in fieldNode.ChildNodes)
			//		{
			//			f.Header = ci.Name;
			//			this.Fields.Add(f);
			//		}
			//	}
			//	else
			//	{
			//		f.Header = fieldNode.Name;
			//		this.Fields.Add(f);
			//	}
			//}

			//foreach(XmlNode node in this.RootNode.ChildNodes)
			//{
			//	Field f = new Field();
			//	f.Header = node.Attributes[0].Name;
			//	string id = node.Attributes.GetNamedItem(f.Header).Value;
			//	string lname = node.SelectSingleNode("au_lname").InnerText;
			//	string fname = node.SelectSingleNode("au_fname").InnerText;
			//	XmlNode contact = node.SelectSingleNode("contact");
			//	string phone = contact.SelectSingleNode("phone").InnerText;
			//	string stAddress = contact.SelectSingleNode("address").InnerText;
			//	string city = contact.SelectSingleNode("city").InnerText;
			//	string state = contact.SelectSingleNode("state").InnerText;
			//	string zip = contact.SelectSingleNode("zip").InnerText;
			//	bool contract = Convert.ToBoolean(int.Parse(node.SelectSingleNode("contract").InnerText));

			//	Author a = new Author(id, lname, fname, phone, stAddress, city, state, zip, contract);
			//	this.Authors.Add(a);
			//}

			return this.Authors;
		}

		public void AddNew(string id, string lastName, string firstName, string phoneNumber, string streetAddress, string city,
			string state, string zipCode, bool isContracted)
		{
			Author a = new Author(id, lastName, firstName, phoneNumber, streetAddress, city, state, zipCode, isContracted);
			this.Authors.Add(a);
			object[] row = { id, lastName, firstName, phoneNumber, streetAddress, city, state, zipCode, isContracted };
			DataAccessSQL.DataWriter.AppendRow(row.ToList());
		}

		public List<string> ExportAll()
		{
			throw new NotImplementedException();
		}

		public bool RemoveById(string id)
		{
			if(!DataAccessSQL.DataWriter.RemoveById(id))
			{
				return false;
			}

			foreach(Author a in this.Authors)
			{
				if(a.Id == id)
				{
					this.Authors.Remove(a);
					break;
				}
			}

			return true;
		}
	}
}
