﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAccessSQL
{
	public static class DataReader
	{
		public static List<List<object>> ReadSqlData(out List<string> cols)
		{
			cols = new List<string>();
			List<List<object>> objs = new List<List<object>>();
			using(SqlConnection connection = new SqlConnection(Const.CONNECTION_STRING))
			{
				connection.Open();
				//connection.ChangeDatabase("pubs");

				//SqlCommand cmd = new SqlCommand("use pubs", connection);
				SqlCommand cmd;
				//cmd.ExecuteNonQuery();
				//cmd.Dispose();

				using(cmd = new SqlCommand("use pubs", connection))
				{
					cmd.ExecuteNonQuery();

				}

				using(cmd = new SqlCommand("select * from authors", connection))
				{
					//int nrows = cmd.ExecuteNonQuery();
					//MessageBox.Show(nrows.ToString());
					using(SqlDataReader reader = cmd.ExecuteReader())
					{
						//object[] values = new object[50];
						//reader.GetValues(values);
						bool first = true;
						while(reader.Read())
						{
							if(first)
							{
								for(int i = 0; i < reader.FieldCount; i++)
								{
									cols.Add(reader.GetName(i));
								}
								first = false;
							}
							//int ord = reader.GetOrdinal("au_fname");
							object[] outarr = new object[reader.FieldCount];
							reader.GetValues(outarr);
							objs.Add(outarr.ToList());
						}
					}
				}
			}
			return objs;
		}
	}
}
