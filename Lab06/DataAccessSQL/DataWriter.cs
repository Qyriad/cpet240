﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataAccessSQL
{
	public struct Const
	{
		public const string CONNECTION_STRING = "Data Source=YUKI;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
	}

	public static class DataWriter
	{ 
		public static void AppendRow(List<object> row)
		{
			int c = Convert.ToInt32((bool) row[8]);
			using(SqlConnection connection = new SqlConnection(Const.CONNECTION_STRING))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				string cmdstr = $"insert into authors values(" +
					$"'{row[0]}', '{row[1]}', '{row[2]}', '{row[3]}', '{row[4]}', " +
					$"'{row[5]}', '{row[6]}', '{row[7]}', {c})";

				using(SqlCommand cmd = new SqlCommand(cmdstr, connection))
				{
					cmd.ExecuteNonQuery();
				}
			}
		}

		public static bool RemoveById(string id)
		{
			Regex idRegex = new Regex(@"\d\d\d-\d\d-\d\d\d\d");
			if(!idRegex.IsMatch(id))
			{
				throw new ArgumentException(@"ID must match pattern \d\d\d-\d\d-\d\d\d\d");
			}

			using(SqlConnection connection = new SqlConnection(Const.CONNECTION_STRING))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				using(SqlCommand cmd = new SqlCommand($"delete from authors where au_id='{id}'", connection))
				{
					try
					{
						cmd.ExecuteNonQuery();
					}
					catch(SqlException e)
					{
						if(e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
						{
							return false;
						}
						// Else rethrow
						throw;
					}
				}

				return true;
			}
		}

		private enum FieldHeader
		{
			au_id = 0, au_lname = 1, au_fname = 2, phone = 3, address = 4, city = 5, state = 6, zip = 7, contract = 8
		}

		public static bool UpdateRow(string idToUpdate, List<object> newRow)
		{
			if(newRow.Count > 9)
			{
				throw new ArgumentException("Rows cannot have more than 9 elements");
			}

			//FieldHeader h = (FieldHeader) fieldIndex;
			//string field = h.ToString();

			using(SqlConnection connection = new SqlConnection(Const.CONNECTION_STRING))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				int contract = Convert.ToInt16((bool) newRow[8]);
				string cmdString = $"update authors set au_id = '{newRow[0]}', au_lname = '{newRow[1]}', " +
					$"au_fname = '{newRow[2]}', phone = '{newRow[3]}', address = '{newRow[4]}', city = '{newRow[5]}'," +
					$"state = '{newRow[6]}', zip = '{newRow[7]}', contract = {contract} where au_id = '{idToUpdate}'";
				using(SqlCommand cmd = new SqlCommand(cmdString, connection))
				{
					try
					{
						cmd.ExecuteNonQuery();
					}
					catch(SqlException e)
					{
						if(e.Message.ToLower().Contains("constraint"))
						{
							return false;
						}
						// Rethrow otherwise
						throw; 
					}
				}
			}

			return true;
		}
	}
}
