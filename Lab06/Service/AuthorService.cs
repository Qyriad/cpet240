﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qyriad.Lab06.DataModel;
using Qyriad.Lab06.Repository;

namespace Qyriad.Lab06.Service
{
	// This is excessive abstraction
	// Author objects are easily displayable
	// For a program of this size a Service is unnecessary
	// But I understand the theoretical academic value

	public struct AuthorViewModel 
	{
		public string Id { get; private set; }
		public string FirstName;
		public string LastName;
		public string FullName
		{
			get => this.FirstName + " " + this.LastName;
		}
		public string PhoneNumber;
		public string StreetAddress;
		public string City;
		public string State;
		public string ZipCode;
		public string Contracted;

		public AuthorViewModel(string id, string lname, string fname, string phoneNumber, string address, string city, string state, string zip, string contracted)
		{
			this.Id = id;
			this.LastName = lname;
			this.FirstName = fname;
			this.PhoneNumber = phoneNumber;
			this.StreetAddress = address;
			this.City = city;
			this.State = state;
			this.ZipCode = zip;
			this.Contracted = contracted;
		}
	}

	public struct AuthorService
	{
		private AuthorRepository _repo;

		public AuthorService(AuthorRepository repo)
		{
			this._repo = repo;
		}

		public List<AuthorViewModel> GetAllAuthors()
		{
			List<AuthorViewModel> authorViews = new List<AuthorViewModel>();
			List<Author> authors = this._repo.FindAll();

			foreach(Author a in authors)
			{
				authorViews.Add(new AuthorViewModel(
					a.Id, a.LastName, a.FirstName, a.PhoneNumber, a.StreetAddress, a.City, a.State, a.ZipCode, a.IsContracted.ToString()));
			}

			return authorViews;
		}
	}
}
