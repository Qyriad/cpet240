﻿// File:	DataWriter.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System.Collections.Generic;
using System.IO;

namespace Qyriad.Lab06.DataAccessTXT
{
	public class DataWriter
	{
		public static void WriteData(ICollection<string> lines, string file)
		{
			using(StreamWriter sw = new StreamWriter(file))
			{
				foreach(string line in lines)
				{
					sw.WriteLine(line);
				}
			}
		}
	}
}
