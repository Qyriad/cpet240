﻿// File:	DataReader.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Qyriad.Lab06.DataAccessTXT
{
    public class DataReader
	{
		public static string[] ReadData()
		{
			string path = ConfigurationManager.AppSettings.Get("filepath");
			List<string> data = new List<string>();

			using (StreamReader sr = new StreamReader(path))
			{
				while(!sr.EndOfStream)
				{
					data.Add(sr.ReadLine());
				}
			}

			return data.ToArray();
		}
	}
}
