﻿// File:	DataReader.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System.Configuration;
using System.Xml;

namespace Qyriad.Lab06.DataAccessXML
{
	public class DataReader
	{
		/// <summary>
		/// Reads the XML file specified in the config and returns the root node
		/// </summary>
		/// <returns>The root node, or null if the file wasn't found or a valid XML file</returns>
		public static XmlElement ReadXmlData()
		{
			string path = ConfigurationManager.AppSettings.Get("filepath");
			XmlDocument doc = new XmlDocument();
			try
			{
				doc.Load(path);
			}
			catch(System.IO.FileNotFoundException)
			{
				return null;
			}

			return doc.DocumentElement;
		}
	}
}
