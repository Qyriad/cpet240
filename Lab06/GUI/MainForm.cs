﻿// File:	MainForm.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

using Qyriad.Lab06.DataModel;
using Qyriad.Lab06.Repository;

namespace Qyriad.Lab06.GUI
{
	public partial class MainForm : Form
	{
		//private Interpreter Inter;
		private AuthorRepository Repo;
		public MainForm()
		{
			InitializeComponent();

			// Make right click select work properly
			this.tvMain.NodeMouseClick += (sender, args) => this.tvMain.SelectedNode = args.Node;
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			List<List<object>> data = DataAccessSQL.DataReader.ReadSqlData(out List<string> cols);
			this.Repo = new AuthorRepository(data, cols);
			// Read and model data
			//XmlElement el = DataAccessXML.DataReader.ReadXmlData();
			
			//this.Inter = new Interpreter(el);
			//this.Repo = new AuthorRepository(el);
			List<Author> authors = this.Repo.FindAll();

			this.lvMain.View = View.Details;

			foreach(AuthorRepository.Field col in this.Repo.Fields) // Add all detected fields
			{
				this.lvMain.Columns.Add(col.Header);
			}

			foreach(Author a in authors)
			{
				this.lvMain.Items.Add(new ListViewItem(a.ToStringArray()));
			}

			foreach(ColumnHeader c in this.lvMain.Columns)
			{
				c.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
			}

			foreach(Author a in this.Repo.Authors)
			{
				TreeNode n = new TreeNode($"{a.LastName}, {a.FirstName}");
				n.Nodes.Add(a.Id);
				TreeNode address = new TreeNode("Address");
				address.Nodes.Add(a.StreetAddress);
				address.Nodes.Add(a.City);
				address.Nodes.Add(a.State);
				address.Nodes.Add(a.ZipCode);
				n.Nodes.Add(address);
				n.Nodes.Add(a.PhoneNumber);
				n.Nodes.Add(a.IsContracted ? "Contracted" : "Not Contracted");

				this.tvMain.Nodes.Add(n);
			}
		}

		private void addToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewAuthorDialog dlg = new NewAuthorDialog();
			dlg.btnAdd.Click += delegate(object sender1, EventArgs e1)
			{
				Regex idRegex = new Regex(@"\d\d\d-\d\d-\d\d\d\d");
				string id = dlg.txtBoxId.Text;
				string lname = dlg.txtBoxLastName.Text;
				string fname = dlg.txtBoxFirstName.Text;
				string phone = dlg.txtBoxPhoneNumber.Text;
				string stAddress = dlg.txtBoxAddress.Text;
				string city = dlg.txtBoxCity.Text;
				string state = dlg.txtBoxState.Text;
				Regex zipRegex = new Regex(@"\d\d\d\d\d");
				string zip = dlg.txtBoxZipCode.Text;
				bool isContracted = dlg.chkBxContracted.Checked;
				string contract = isContracted.ToString();

				if(!idRegex.IsMatch(id))
				{
					MessageBox.Show("Please enter an id like nnn-nn-nnnn, where n is any digit");
					return;
				}

				if(!zipRegex.IsMatch(zip))
				{
					MessageBox.Show("Please enter a zip code like nnnnn, where n is any digit");
					return;
				}

				string[] s =
				{
					id, lname, fname, phone, stAddress, city, state, zip, contract
				};

				this.lvMain.Items.Insert(0, new ListViewItem(s));

				{
					TreeNode n = new TreeNode($"{lname}, {fname}");
					n.Nodes.Add(id);
					TreeNode address = new TreeNode("Address");
					address.Nodes.Add(stAddress);
					address.Nodes.Add(city);
					address.Nodes.Add(state);
					address.Nodes.Add(zip);
					n.Nodes.Add(address);
					n.Nodes.Add(phone);
					n.Nodes.Add(isContracted ? "Contracted" : "Not Contracted");
					this.tvMain.Nodes.Insert(0, n);
				}

				this.Repo.AddNew(id, lname, fname, phone, stAddress, city, state, zip, isContracted);
			};
			dlg.Show();
		}

		private void commitChangesMenuItem_Click(object sender, EventArgs e)
		{
			//
		}

		private void lvMain_MouseClick(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				if(lvMain.FocusedItem.Bounds.Contains(e.Location) == true)
				{
					contextMenuStripListView.Show(Cursor.Position);
				}
			}
		}

		private void removeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult r = MessageBox.Show("Are you sure you want to delete this item?", "Confirm Delete", MessageBoxButtons.OKCancel);
			if(r == DialogResult.OK)
			{
				string id = this.lvMain.FocusedItem.SubItems[0].Text;
				if(!this.Repo.RemoveById(id))
				{
					MessageBox.Show("You cannot remove this author", "Error");
					return;
				}

				this.lvMain.Items.RemoveAt(this.lvMain.FocusedItem.Index);
				for(int i = 0; i < this.tvMain.Nodes.Count; i++)
				{
					if(this.tvMain.Nodes[i].Nodes[0].Text == id)
					{
						this.tvMain.Nodes[i].Remove();
					}
				}
			}
		}

		private void tvMain_MouseClick(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				if(this.tvMain.SelectedNode.Level == 0)
				{
					contextMenuStripTreeView.Show(Cursor.Position);
				}
			}
		}

		private void removeToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			DialogResult r = MessageBox.Show("Are you sure you want to delete this item?", "Confirm Delete", MessageBoxButtons.OKCancel);
			if(r == DialogResult.OK)
			{
				string id = this.tvMain.SelectedNode.Nodes[0].Text;
				this.Repo.RemoveById(id);
				for(int i = 0; i < this.lvMain.Items.Count; i++)
				{
					if(this.lvMain.Items[i].SubItems[0].Text == id)
					{
						this.lvMain.Items[i].Remove();
					}
				}
				this.tvMain.SelectedNode.Remove();
			}
		}

		private void lvMain_DoubleClick(object sender, EventArgs e)
		{
			this.lvMain.FocusedItem.BeginEdit();
			string id = this.lvMain.FocusedItem.SubItems[0].Text;
			string lname = this.lvMain.FocusedItem.SubItems[1].Text;
			string fname = this.lvMain.FocusedItem.SubItems[2].Text;
			string phone = this.lvMain.FocusedItem.SubItems[3].Text;
			string address = this.lvMain.FocusedItem.SubItems[4].Text;
			string city = this.lvMain.FocusedItem.SubItems[5].Text;
			string state = this.lvMain.FocusedItem.SubItems[6].Text;
			string zip = this.lvMain.FocusedItem.SubItems[7].Text;
			string contract = this.lvMain.FocusedItem.SubItems[8].Text;
		}

		private void editToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewAuthorDialog update = new NewAuthorDialog();
			update.btnAdd.Text = "Update";
			update.Text = "Update Author";
			var sub = this.lvMain.FocusedItem.SubItems;
			string id = sub[0].Text;
			string lname = sub[1].Text;
			string fname = sub[2].Text;
			string phone = sub[3].Text;
			string address = sub[4].Text;
			string city = sub[5].Text;
			string state = sub[6].Text;
			string zip = sub[7].Text;
			bool isContracted;
			if(sub[8].Text == "True")
			{
				isContracted = true;
			}
			else
			{
				isContracted = false;
			}

			update.txtBoxId.Text = id;
			update.txtBoxLastName.Text = lname;
			update.txtBoxFirstName.Text = fname;
			update.txtBoxPhoneNumber.Text = phone;
			update.txtBoxAddress.Text = address;
			update.txtBoxCity.Text = city;
			update.txtBoxState.Text = state;
			update.txtBoxZipCode.Text = zip;
			update.chkBxContracted.Checked = isContracted;

			update.btnAdd.Click += (o, args) =>
			{
				string newId = update.txtBoxId.Text;
				Regex idRegex = new Regex(@"\d\d\d-\d\d-\d\d\d\d");
				if(!idRegex.IsMatch(newId))
				{
					MessageBox.Show("Please enter an id like nnn-nn-nnnn, where n is any digit", "Error");
					return;
				}
				lname = update.txtBoxLastName.Text;
				fname = update.txtBoxFirstName.Text;
				phone = update.txtBoxPhoneNumber.Text;
				address = update.txtBoxAddress.Text;
				city = update.txtBoxCity.Text;
				state = update.txtBoxState.Text;
				zip = update.txtBoxZipCode.Text;
				Regex zipRegex = new Regex(@"\d\d\d\d\d");
				if(!zipRegex.IsMatch(zip))
				{
					MessageBox.Show("Please enter a zip code like nnnnn, where n is any digit", "Error");
					return;
				}
				isContracted = update.chkBxContracted.Checked;
				object[] row = {newId, lname, fname, phone, address, city, state, zip, isContracted};
				if(!DataAccessSQL.DataWriter.UpdateRow(id, row.ToList()))
				{
					MessageBox.Show("Invalid update data");
				}
				update.Close();

				// Update the list view
				sub[0].Text = newId;
				sub[1].Text = lname;
				sub[2].Text = fname;
				sub[3].Text = phone;
				sub[4].Text = address;
				sub[5].Text = city;
				sub[6].Text = state;
				sub[7].Text = zip;
				sub[8].Text = isContracted ? "Contracted" : "Not Contracted";

				// Update the tree view
				TreeNode root = null;
				foreach(TreeNode n in this.tvMain.Nodes)
				{
					if(n.Nodes[0].Text == id)
					{
						root = n;
						break;
					}
				}

				if(root == null)
				{
					throw new Exception("What. That shouldn't be possible");
				}

				root.Text = $"{lname}, {fname}";
				root.Nodes[0].Text = newId;
				TreeNode contact = root.Nodes[1];
				contact.Nodes[0].Text = address;
				contact.Nodes[1].Text = city;
				contact.Nodes[2].Text = state;
				contact.Nodes[3].Text = zip;
				root.Nodes[2].Text = phone;
				root.Nodes[3].Text = isContracted ? "Contracted" : "Not Contracted";
			};

			update.Show();
		}

		private void editToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			NewAuthorDialog update = new NewAuthorDialog();
			update.btnAdd.Text = "Update";
			update.Text = "Update Author";
			var node = this.tvMain.SelectedNode;
			string id = node.Nodes[0].Text;
			int commaPos = node.Text.IndexOf(",");
			string lname = node.Text.Substring(0, commaPos);
			string fname = node.Text.Substring(commaPos + 2);
			string phone = node.Nodes[2].Text;
			TreeNode contact = node.Nodes[1];
			string address = contact.Nodes[0].Text;
			string city = contact.Nodes[1].Text;
			string state = contact.Nodes[2].Text;
			string zip = contact.Nodes[3].Text;
			bool isContracted;
			if(node.Nodes[3].Text == "Contracted")
			{
				isContracted = true;
			}
			else
			{
				isContracted = false;
			}

			update.txtBoxId.Text = id;
			update.txtBoxLastName.Text = lname;
			update.txtBoxFirstName.Text = fname;
			update.txtBoxPhoneNumber.Text = phone;
			update.txtBoxAddress.Text = address;
			update.txtBoxCity.Text = city;
			update.txtBoxState.Text = state;
			update.txtBoxZipCode.Text = zip;
			update.chkBxContracted.Checked = isContracted;

			update.btnAdd.Click += (o, args) =>
			{
				string newId = update.txtBoxId.Text;
				Regex idRegex = new Regex(@"\d\d\d-\d\d-\d\d\d\d");
				if(!idRegex.IsMatch(newId))
				{
					MessageBox.Show("Please enter an id like nnn-nn-nnnn, where n is any digit", "Error");
					return;
				}
				lname = update.txtBoxLastName.Text;
				fname = update.txtBoxFirstName.Text;
				phone = update.txtBoxPhoneNumber.Text;
				address = update.txtBoxAddress.Text;
				city = update.txtBoxCity.Text;
				state = update.txtBoxState.Text;
				zip = update.txtBoxZipCode.Text;
				Regex zipRegex = new Regex(@"\d\d\d\d\d");
				if(!zipRegex.IsMatch(zip))
				{
					MessageBox.Show("Please enter a zip code like nnnnn, where n is any digit", "Error");
					return;
				}
				isContracted = update.chkBxContracted.Checked;
				object[] row = { newId, lname, fname, phone, address, city, state, zip, isContracted };
				if(!DataAccessSQL.DataWriter.UpdateRow(id, row.ToList()))
				{
					MessageBox.Show("Invalid update data");
				}
				update.Close();

				// Update the tree view
				node.Text = $"{lname}, {fname}";
				node.Nodes[0].Text = newId;
				contact.Nodes[0].Text = address;
				contact.Nodes[1].Text = city;
				contact.Nodes[2].Text = state;
				contact.Nodes[3].Text = zip;
				node.Nodes[2].Text = phone;
				node.Nodes[3].Text = isContracted ? "Contracted" : "Not Contracted";

				ListViewItem item = null;
				// Update the list view
				foreach(ListViewItem i in this.lvMain.Items)
				{
					if(i.SubItems[0].Text == id)
					{
						item = i;
						break;
					}
				}
				
				if(item == null)
				{
					throw new Exception("What. That shouldn't be possible");
				}

				item.SubItems[0].Text = newId;
				item.SubItems[1].Text = lname;
				item.SubItems[2].Text = fname;
				item.SubItems[3].Text = phone;
				item.SubItems[4].Text = address;
				item.SubItems[5].Text = city;
				item.SubItems[6].Text = state;
				item.SubItems[7].Text = zip;
				item.SubItems[8].Text = isContracted ? "Contracted" : "Not Contracted";
			};

			update.Show();
		}
	}
}
