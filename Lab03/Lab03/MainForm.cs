﻿// File:	MainForm.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab03
// Date:	09/22/2017
// License: MIT

using System;
using System.Windows.Forms;

namespace Lab03
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void btnEnterData_Click(object sender, EventArgs e)
		{
			EnterDataForm enterDataForm = new EnterDataForm();
			enterDataForm.Calculate += EnterDataForm_Calculate;
			enterDataForm.ShowDialog();
		}

		private void EnterDataForm_Calculate(object sender, EnterDataForm.CalculateEventArgs eventArgs)
		{
			// String interpolation is basically just string.Format(..)
			const double taxRate = 0.15;
			double gross = eventArgs.Wage * eventArgs.HoursWorked;
			double tax = gross * taxRate;
			double net = gross - tax;
			string s = $"Name:\t\t{eventArgs.FullName}{Environment.NewLine}Hours worked:\t{eventArgs.HoursWorked:F2} hours{Environment.NewLine}" +
						$"Wage rate:\t{eventArgs.Wage:C}/hr{Environment.NewLine}Gross pay:\t{gross:C}{Environment.NewLine}" +
						$"Taxes:\t\t{tax:C}{Environment.NewLine}Net pay:\t\t{net:C}{Environment.NewLine}{Environment.NewLine}";
			this.txtBoxData.AppendText(s);
		}
	}
}
