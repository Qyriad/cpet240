﻿// File:	Program.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab03
// Date:	09/22/2017
// License: MIT

using System;
using System.Windows.Forms;

namespace Lab03
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
	}
}
