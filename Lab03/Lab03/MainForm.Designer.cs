﻿// File:	MainForm.Designer.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab03
// Date:	09/22/2017
// License: MIT

namespace Lab03
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBoxData = new System.Windows.Forms.TextBox();
			this.btnEnterData = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtBoxData
			// 
			this.txtBoxData.Location = new System.Drawing.Point(12, 42);
			this.txtBoxData.Multiline = true;
			this.txtBoxData.Name = "txtBoxData";
			this.txtBoxData.ReadOnly = true;
			this.txtBoxData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtBoxData.Size = new System.Drawing.Size(260, 207);
			this.txtBoxData.TabIndex = 0;
			this.txtBoxData.WordWrap = false;
			// 
			// btnEnterData
			// 
			this.btnEnterData.Location = new System.Drawing.Point(13, 13);
			this.btnEnterData.Name = "btnEnterData";
			this.btnEnterData.Size = new System.Drawing.Size(75, 23);
			this.btnEnterData.TabIndex = 1;
			this.btnEnterData.Text = "Enter Data";
			this.btnEnterData.UseVisualStyleBackColor = true;
			this.btnEnterData.Click += new System.EventHandler(this.btnEnterData_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnEnterData);
			this.Controls.Add(this.txtBoxData);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Main Form";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtBoxData;
		private System.Windows.Forms.Button btnEnterData;
	}
}

