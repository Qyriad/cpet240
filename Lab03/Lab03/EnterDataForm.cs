﻿// File:	EnterDataForm.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab03
// Date:	09/22/2017
// License: MIT

using System;
using System.Media;
using System.Windows.Forms;

namespace Lab03
{
	public partial class EnterDataForm : Form
	{
		public class CalculateEventArgs : EventArgs
		{
			public double Wage;
			public double HoursWorked;
			public string FullName;

			public CalculateEventArgs(double wage, double hoursWorked, string fullName)
			{
				this.Wage = wage;
				this.HoursWorked = hoursWorked;
				this.FullName = fullName;
			}
		}

		public delegate void CalculateHandler(EnterDataForm btn, CalculateEventArgs e);

		public event CalculateHandler Calculate;
		public Button CalculateButton { get => this.btnCalculate; }

		public RadioButton CheckedButton
		{
			get
			{
				// Supposedly LINQ would work too but I don't really understand it so...
				// return this.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked); <-- LINQ version
				// IEnumerable<RadioButton> x = from ctrl in this.Controls.OfType<RadioButton>() where ctrl.Checked select ctrl;
				foreach(object control in this.grpBoxWageRate.Controls)
				{
					if(control is RadioButton r && r.Checked)
					{
						return r;
					}
				}
				return null;
			}
		}

		public string OtherWage
		{
			get => this.txtBoxWage.Text;
		}

		public EnterDataForm()
		{
			InitializeComponent();
		}

		private void radioBtnOther_CheckedChanged(object sender, EventArgs e)
		{
			if(sender is RadioButton radioBtn)
			{
				if(radioBtn.Checked)
				{
					this.txtBoxWage.Show();
				}
				else
				{
					this.txtBoxWage.Hide();
				}
			}
		}

		private void btnCalculate_Click(object sender, EventArgs e)
		{
			double wage, hours;
			// Validate other wage if applicable
			if(this.CheckedButton == this.radioBtnOther)
			{
				string s = this.txtBoxWage.Text.Replace("&", "").Replace("$", "");
				if(!double.TryParse(s, out wage))
				{
					SystemSounds.Beep.Play();
					this.errorProvider.SetError(this.txtBoxWage, "Please enter a number");
					return;
				}
			}
			else
			{
				string s = this.CheckedButton.Text.Replace("&", "").Replace("$", "");
				if(!double.TryParse(s, out wage))
				{
					SystemSounds.Beep.Play();
					throw new Exception("This shouldn't even be possible. ");
				}
			}

			// Validate hours worked
			if(!double.TryParse(this.txtBoxHoursWorked.Text, out hours))
			{
				SystemSounds.Beep.Play();
				this.errorProvider.SetError(this.txtBoxHoursWorked, "Please enter a number");
				return;
			}

			// Validate name
			if(this.txtBoxFullName.Text == string.Empty)
			{
				SystemSounds.Beep.Play();
				this.errorProvider.SetError(this.txtBoxFullName, "Please enter the name");
				return; 
			}

			this.errorProvider.Clear();

			// If we got here we're good! Fire the calculate event!
			Calculate?.Invoke(this, new CalculateEventArgs(wage, hours, this.txtBoxFullName.Text));
		}

		private void btnSuspend_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}