﻿// File:	EnterDataForm.Designer.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab03
// Date:	09/22/2017
// License: MIT

namespace Lab03
{
	partial class EnterDataForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnterDataForm));
			this.grpBoxWageRate = new System.Windows.Forms.GroupBox();
			this.txtBoxWage = new System.Windows.Forms.TextBox();
			this.radioBtnOther = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioBtn1000 = new System.Windows.Forms.RadioButton();
			this.radioBtn933 = new System.Windows.Forms.RadioButton();
			this.radioBtn875 = new System.Windows.Forms.RadioButton();
			this.lblHoursWorked = new System.Windows.Forms.Label();
			this.txtBoxHoursWorked = new System.Windows.Forms.TextBox();
			this.lblFullName = new System.Windows.Forms.Label();
			this.txtBoxFullName = new System.Windows.Forms.TextBox();
			this.btnCalculate = new System.Windows.Forms.Button();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.btnSuspend = new System.Windows.Forms.Button();
			this.grpBoxWageRate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// grpBoxWageRate
			// 
			this.grpBoxWageRate.Controls.Add(this.txtBoxWage);
			this.grpBoxWageRate.Controls.Add(this.radioBtnOther);
			this.grpBoxWageRate.Controls.Add(this.radioButton1);
			this.grpBoxWageRate.Controls.Add(this.radioBtn1000);
			this.grpBoxWageRate.Controls.Add(this.radioBtn933);
			this.grpBoxWageRate.Controls.Add(this.radioBtn875);
			resources.ApplyResources(this.grpBoxWageRate, "grpBoxWageRate");
			this.grpBoxWageRate.Name = "grpBoxWageRate";
			this.grpBoxWageRate.TabStop = false;
			// 
			// txtBoxWage
			// 
			resources.ApplyResources(this.txtBoxWage, "txtBoxWage");
			this.txtBoxWage.Name = "txtBoxWage";
			// 
			// radioBtnOther
			// 
			resources.ApplyResources(this.radioBtnOther, "radioBtnOther");
			this.radioBtnOther.Name = "radioBtnOther";
			this.radioBtnOther.TabStop = true;
			this.radioBtnOther.UseVisualStyleBackColor = true;
			this.radioBtnOther.CheckedChanged += new System.EventHandler(this.radioBtnOther_CheckedChanged);
			// 
			// radioButton1
			// 
			resources.ApplyResources(this.radioButton1, "radioButton1");
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.TabStop = true;
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// radioBtn1000
			// 
			resources.ApplyResources(this.radioBtn1000, "radioBtn1000");
			this.radioBtn1000.Name = "radioBtn1000";
			this.radioBtn1000.TabStop = true;
			this.radioBtn1000.UseVisualStyleBackColor = true;
			// 
			// radioBtn933
			// 
			resources.ApplyResources(this.radioBtn933, "radioBtn933");
			this.radioBtn933.Name = "radioBtn933";
			this.radioBtn933.TabStop = true;
			this.radioBtn933.UseVisualStyleBackColor = true;
			// 
			// radioBtn875
			// 
			resources.ApplyResources(this.radioBtn875, "radioBtn875");
			this.radioBtn875.Name = "radioBtn875";
			this.radioBtn875.TabStop = true;
			this.radioBtn875.UseVisualStyleBackColor = true;
			// 
			// lblHoursWorked
			// 
			resources.ApplyResources(this.lblHoursWorked, "lblHoursWorked");
			this.lblHoursWorked.Name = "lblHoursWorked";
			// 
			// txtBoxHoursWorked
			// 
			resources.ApplyResources(this.txtBoxHoursWorked, "txtBoxHoursWorked");
			this.txtBoxHoursWorked.Name = "txtBoxHoursWorked";
			// 
			// lblFullName
			// 
			resources.ApplyResources(this.lblFullName, "lblFullName");
			this.lblFullName.Name = "lblFullName";
			// 
			// txtBoxFullName
			// 
			resources.ApplyResources(this.txtBoxFullName, "txtBoxFullName");
			this.txtBoxFullName.Name = "txtBoxFullName";
			// 
			// btnCalculate
			// 
			resources.ApplyResources(this.btnCalculate, "btnCalculate");
			this.btnCalculate.Name = "btnCalculate";
			this.btnCalculate.UseVisualStyleBackColor = true;
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// btnSuspend
			// 
			resources.ApplyResources(this.btnSuspend, "btnSuspend");
			this.btnSuspend.Name = "btnSuspend";
			this.btnSuspend.UseVisualStyleBackColor = true;
			this.btnSuspend.Click += new System.EventHandler(this.btnSuspend_Click);
			// 
			// EnterDataForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnSuspend);
			this.Controls.Add(this.btnCalculate);
			this.Controls.Add(this.txtBoxFullName);
			this.Controls.Add(this.lblFullName);
			this.Controls.Add(this.txtBoxHoursWorked);
			this.Controls.Add(this.lblHoursWorked);
			this.Controls.Add(this.grpBoxWageRate);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "EnterDataForm";
			this.grpBoxWageRate.ResumeLayout(false);
			this.grpBoxWageRate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox grpBoxWageRate;
		private System.Windows.Forms.RadioButton radioBtn875;
		private System.Windows.Forms.RadioButton radioBtn933;
		private System.Windows.Forms.RadioButton radioBtn1000;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioBtnOther;
		private System.Windows.Forms.TextBox txtBoxWage;
		private System.Windows.Forms.Label lblHoursWorked;
		private System.Windows.Forms.TextBox txtBoxHoursWorked;
		private System.Windows.Forms.Label lblFullName;
		private System.Windows.Forms.TextBox txtBoxFullName;
		private System.Windows.Forms.Button btnCalculate;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.Button btnSuspend;
	}
}