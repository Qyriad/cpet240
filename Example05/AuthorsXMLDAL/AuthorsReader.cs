﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AuthorsXMLDAL
{
    public class AuthorsReader
    {
		public string filepath { get; private set; }
		public XmlDocument xmldoc { get; private set; }
		public AuthorsReader(string filename)
		{
			this.filepath = filename;
			Console.WriteLine(this.filepath);
			xmldoc.Load(this.filepath);
		}

		List<XmlNode> GetAllAuthors()
		{
			return null;
		}

		XmlNode FindById(string au_id)
		{
			return null;
		}

		// Create()
		bool Create(string au_id, string state)
		{
			XmlNode author = this.xmldoc.CreateElement("author");
			XmlNode contact = this.xmldoc.CreateElement("contact");
			XmlAttribute id = this.xmldoc.CreateAttribute("au_id");

			id.InnerText = au_id;
			author.Attributes.Append(id);

			contact.AppendChild(this.xmldoc.CreateElement("state"));
			contact[state].InnerText = state;

			author.AppendChild(contact);

			XmlNode root = xmldoc.SelectSingleNode("authors");

			root.AppendChild(author);
			xmldoc.Save(this.filepath);

			return false;
		}
		// Remove()
		// Update()
    }
}
