﻿namespace Example04
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnShowDialog = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnShowDialog
			// 
			this.btnShowDialog.Location = new System.Drawing.Point(42, 34);
			this.btnShowDialog.Name = "btnShowDialog";
			this.btnShowDialog.Size = new System.Drawing.Size(75, 23);
			this.btnShowDialog.TabIndex = 0;
			this.btnShowDialog.Text = "Show Other Dialog";
			this.btnShowDialog.UseVisualStyleBackColor = true;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnShowDialog);
			this.MinimumSize = new System.Drawing.Size(300, 300);
			this.Name = "frmMain";
			this.Text = "Main Form";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnShowDialog;
	}
}

