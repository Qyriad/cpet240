#include <iostream>

#include <Windows.h>


int main(int argc, char **argv)
{
	UINT flags = MB_ABORTRETRYIGNORE | MB_ICONEXCLAMATION;
	int retVal = MessageBox(NULL, L"Hello", NULL, flags);
	switch(retVal)
	{
		case 0:
		{
			DWORD err = GetLastError();
			std::cout << "Message box error: " << err << std::endl;
			break;
		}
		case IDCANCEL:
			std::cout << "Cancel button pressed" << std::endl;
			break;
		case IDOK:
			std::cout << "Ok button pressed" << std::endl;
			break;
		case IDABORT:
			std::cout << "Abort" << std::endl;
			break;
		case IDRETRY:
			std::cout << "Retry" << std::endl;
			break;
		case IDIGNORE:
			std::cout << "Ignore" << std::endl;
			break;
		default:
			std::cout << "Other" << std::endl;
	}
	return 0;
}