﻿// File:	NewAuthorDialog.Designer.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

namespace Qyriad.Lab05.GUI
{
	partial class NewAuthorDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblId = new System.Windows.Forms.Label();
			this.txtBoxId = new System.Windows.Forms.TextBox();
			this.lblLastName = new System.Windows.Forms.Label();
			this.txtBoxLastName = new System.Windows.Forms.TextBox();
			this.lblFirstName = new System.Windows.Forms.Label();
			this.txtBoxFirstName = new System.Windows.Forms.TextBox();
			this.lblPhoneNumber = new System.Windows.Forms.Label();
			this.txtBoxPhoneNumber = new System.Windows.Forms.TextBox();
			this.lblAddress = new System.Windows.Forms.Label();
			this.txtBoxAddress = new System.Windows.Forms.TextBox();
			this.lblZipCode = new System.Windows.Forms.Label();
			this.txtBoxZipCode = new System.Windows.Forms.TextBox();
			this.lblContracted = new System.Windows.Forms.Label();
			this.chkBxContracted = new System.Windows.Forms.CheckBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblCity = new System.Windows.Forms.Label();
			this.txtBoxCity = new System.Windows.Forms.TextBox();
			this.lblState = new System.Windows.Forms.Label();
			this.txtBoxState = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblId
			// 
			this.lblId.AutoSize = true;
			this.lblId.Location = new System.Drawing.Point(12, 9);
			this.lblId.Name = "lblId";
			this.lblId.Size = new System.Drawing.Size(58, 13);
			this.lblId.TabIndex = 0;
			this.lblId.Text = "Author &ID: ";
			// 
			// txtBoxId
			// 
			this.txtBoxId.Location = new System.Drawing.Point(103, 6);
			this.txtBoxId.Name = "txtBoxId";
			this.txtBoxId.Size = new System.Drawing.Size(100, 20);
			this.txtBoxId.TabIndex = 1;
			// 
			// lblLastName
			// 
			this.lblLastName.AutoSize = true;
			this.lblLastName.Location = new System.Drawing.Point(12, 35);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Size = new System.Drawing.Size(64, 13);
			this.lblLastName.TabIndex = 2;
			this.lblLastName.Text = "&Last Name: ";
			// 
			// txtBoxLastName
			// 
			this.txtBoxLastName.Location = new System.Drawing.Point(103, 32);
			this.txtBoxLastName.Name = "txtBoxLastName";
			this.txtBoxLastName.Size = new System.Drawing.Size(100, 20);
			this.txtBoxLastName.TabIndex = 3;
			// 
			// lblFirstName
			// 
			this.lblFirstName.AutoSize = true;
			this.lblFirstName.Location = new System.Drawing.Point(12, 61);
			this.lblFirstName.Name = "lblFirstName";
			this.lblFirstName.Size = new System.Drawing.Size(63, 13);
			this.lblFirstName.TabIndex = 4;
			this.lblFirstName.Text = "&First Name: ";
			// 
			// txtBoxFirstName
			// 
			this.txtBoxFirstName.Location = new System.Drawing.Point(103, 58);
			this.txtBoxFirstName.Name = "txtBoxFirstName";
			this.txtBoxFirstName.Size = new System.Drawing.Size(100, 20);
			this.txtBoxFirstName.TabIndex = 5;
			// 
			// lblPhoneNumber
			// 
			this.lblPhoneNumber.AutoSize = true;
			this.lblPhoneNumber.Location = new System.Drawing.Point(12, 86);
			this.lblPhoneNumber.Name = "lblPhoneNumber";
			this.lblPhoneNumber.Size = new System.Drawing.Size(84, 13);
			this.lblPhoneNumber.TabIndex = 6;
			this.lblPhoneNumber.Text = "&Phone Number: ";
			// 
			// txtBoxPhoneNumber
			// 
			this.txtBoxPhoneNumber.Location = new System.Drawing.Point(103, 83);
			this.txtBoxPhoneNumber.Name = "txtBoxPhoneNumber";
			this.txtBoxPhoneNumber.Size = new System.Drawing.Size(100, 20);
			this.txtBoxPhoneNumber.TabIndex = 7;
			// 
			// lblAddress
			// 
			this.lblAddress.AutoSize = true;
			this.lblAddress.Location = new System.Drawing.Point(12, 112);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(51, 13);
			this.lblAddress.TabIndex = 8;
			this.lblAddress.Text = "&Address: ";
			// 
			// txtBoxAddress
			// 
			this.txtBoxAddress.Location = new System.Drawing.Point(103, 109);
			this.txtBoxAddress.Name = "txtBoxAddress";
			this.txtBoxAddress.Size = new System.Drawing.Size(100, 20);
			this.txtBoxAddress.TabIndex = 9;
			// 
			// lblZipCode
			// 
			this.lblZipCode.AutoSize = true;
			this.lblZipCode.Location = new System.Drawing.Point(12, 190);
			this.lblZipCode.Name = "lblZipCode";
			this.lblZipCode.Size = new System.Drawing.Size(56, 13);
			this.lblZipCode.TabIndex = 14;
			this.lblZipCode.Text = "&Zip Code: ";
			// 
			// txtBoxZipCode
			// 
			this.txtBoxZipCode.Location = new System.Drawing.Point(103, 187);
			this.txtBoxZipCode.Name = "txtBoxZipCode";
			this.txtBoxZipCode.Size = new System.Drawing.Size(100, 20);
			this.txtBoxZipCode.TabIndex = 15;
			// 
			// lblContracted
			// 
			this.lblContracted.AutoSize = true;
			this.lblContracted.Location = new System.Drawing.Point(12, 213);
			this.lblContracted.Name = "lblContracted";
			this.lblContracted.Size = new System.Drawing.Size(65, 13);
			this.lblContracted.TabIndex = 16;
			this.lblContracted.Text = "Con&tracted: ";
			// 
			// chkBxContracted
			// 
			this.chkBxContracted.AutoSize = true;
			this.chkBxContracted.Location = new System.Drawing.Point(103, 213);
			this.chkBxContracted.Name = "chkBxContracted";
			this.chkBxContracted.Size = new System.Drawing.Size(15, 14);
			this.chkBxContracted.TabIndex = 17;
			this.chkBxContracted.UseVisualStyleBackColor = true;
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(12, 247);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 18;
			this.btnAdd.Text = "&Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(127, 247);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 19;
			this.btnCancel.Text = "Ca&ncel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblCity
			// 
			this.lblCity.AutoSize = true;
			this.lblCity.Location = new System.Drawing.Point(12, 138);
			this.lblCity.Name = "lblCity";
			this.lblCity.Size = new System.Drawing.Size(24, 13);
			this.lblCity.TabIndex = 10;
			this.lblCity.Text = "&City";
			// 
			// txtBoxCity
			// 
			this.txtBoxCity.Location = new System.Drawing.Point(103, 135);
			this.txtBoxCity.Name = "txtBoxCity";
			this.txtBoxCity.Size = new System.Drawing.Size(100, 20);
			this.txtBoxCity.TabIndex = 11;
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Location = new System.Drawing.Point(12, 164);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(38, 13);
			this.lblState.TabIndex = 12;
			this.lblState.Text = "&State: ";
			// 
			// txtBoxState
			// 
			this.txtBoxState.Location = new System.Drawing.Point(103, 161);
			this.txtBoxState.Name = "txtBoxState";
			this.txtBoxState.Size = new System.Drawing.Size(100, 20);
			this.txtBoxState.TabIndex = 13;
			// 
			// NewAuthorDialog
			// 
			this.AcceptButton = this.btnAdd;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(215, 282);
			this.ControlBox = false;
			this.Controls.Add(this.txtBoxState);
			this.Controls.Add(this.lblState);
			this.Controls.Add(this.txtBoxCity);
			this.Controls.Add(this.lblCity);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.chkBxContracted);
			this.Controls.Add(this.lblContracted);
			this.Controls.Add(this.txtBoxZipCode);
			this.Controls.Add(this.lblZipCode);
			this.Controls.Add(this.txtBoxAddress);
			this.Controls.Add(this.lblAddress);
			this.Controls.Add(this.txtBoxPhoneNumber);
			this.Controls.Add(this.lblPhoneNumber);
			this.Controls.Add(this.txtBoxFirstName);
			this.Controls.Add(this.lblFirstName);
			this.Controls.Add(this.txtBoxLastName);
			this.Controls.Add(this.lblLastName);
			this.Controls.Add(this.txtBoxId);
			this.Controls.Add(this.lblId);
			this.Name = "NewAuthorDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "New Author";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblId;
		internal System.Windows.Forms.TextBox txtBoxId;
		private System.Windows.Forms.Label lblLastName;
		internal System.Windows.Forms.TextBox txtBoxLastName;
		private System.Windows.Forms.Label lblFirstName;
		internal System.Windows.Forms.TextBox txtBoxFirstName;
		private System.Windows.Forms.Label lblPhoneNumber;
		internal System.Windows.Forms.TextBox txtBoxPhoneNumber;
		private System.Windows.Forms.Label lblAddress;
		internal System.Windows.Forms.TextBox txtBoxAddress;
		private System.Windows.Forms.Label lblZipCode;
		internal System.Windows.Forms.TextBox txtBoxZipCode;
		private System.Windows.Forms.Label lblContracted;
		internal System.Windows.Forms.CheckBox chkBxContracted;
		internal System.Windows.Forms.Button btnAdd;
		internal System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblCity;
		internal System.Windows.Forms.TextBox txtBoxCity;
		private System.Windows.Forms.Label lblState;
		internal System.Windows.Forms.TextBox txtBoxState;
	}
}