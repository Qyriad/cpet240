﻿// File:	MainForm.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

using Qyriad.Lab05.DataModel;

namespace Qyriad.Lab05.GUI
{
	public partial class MainForm : Form
	{
		private Interpreter Inter;
		public MainForm()
		{
			InitializeComponent();

			// Make right click select work properly
			this.tvMain.NodeMouseClick += (sender, args) => this.tvMain.SelectedNode = args.Node;
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			// Read and model data
			XmlElement el = DataAccessXML.DataReader.ReadXmlData();
			this.Inter = new Interpreter(el);

			this.lvMain.View = View.Details;

			foreach(Interpreter.Field col in this.Inter.Fields) // Add all detected fields
			{
				this.lvMain.Columns.Add(col.Header);
			}

			foreach(Author a in this.Inter.Authors)
			{
				this.lvMain.Items.Add(new ListViewItem(a.ToStringArray()));
			}

			foreach(ColumnHeader c in this.lvMain.Columns)
			{
				c.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
			}

			foreach(Author a in this.Inter.Authors)
			{
				TreeNode n = new TreeNode($"{a.LastName}, {a.FirstName}");
				n.Nodes.Add(a.Id);
				TreeNode address = new TreeNode("Address");
				address.Nodes.Add(a.StreetAddress);
				address.Nodes.Add(a.City);
				address.Nodes.Add(a.State);
				address.Nodes.Add(a.ZipCode);
				n.Nodes.Add(address);
				n.Nodes.Add(a.PhoneNumber);
				n.Nodes.Add(a.IsContracted ? "Contracted" : "Not Contracted");

				this.tvMain.Nodes.Add(n);
			}
		}

		private void addToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewAuthorDialog dlg = new NewAuthorDialog();
			dlg.btnAdd.Click += delegate(object sender1, EventArgs e1)
			{
				string[] s =
				{
					dlg.txtBoxId.Text,
					dlg.txtBoxLastName.Text,
					dlg.txtBoxFirstName.Text,
					dlg.txtBoxPhoneNumber.Text,
					dlg.txtBoxAddress.Text,
					dlg.txtBoxCity.Text,
					dlg.txtBoxState.Text,
					dlg.txtBoxZipCode.Text,
					dlg.chkBxContracted.Checked.ToString()
				};

				this.lvMain.Items.Add(new ListViewItem(s));
				{
					TreeNode n = new TreeNode($"{dlg.txtBoxLastName.Text}, {dlg.txtBoxFirstName.Text}");
					n.Nodes.Add(dlg.txtBoxId.Text);
					TreeNode address = new TreeNode("Address");
					address.Nodes.Add(dlg.txtBoxAddress.Text);
					address.Nodes.Add(dlg.txtBoxCity.Text);
					address.Nodes.Add(dlg.txtBoxState.Text);
					address.Nodes.Add(dlg.txtBoxZipCode.Text);
					n.Nodes.Add(address);
					n.Nodes.Add(dlg.txtBoxPhoneNumber.Text);
					n.Nodes.Add(dlg.chkBxContracted.Checked ? "Contracted" : "Not Contracted");
					this.tvMain.Nodes.Add(n);
				}

				this.Inter.AddNew(dlg.txtBoxId.Text,
					dlg.txtBoxLastName.Text,
					dlg.txtBoxFirstName.Text,
					dlg.txtBoxPhoneNumber.Text,
					dlg.txtBoxAddress.Text,
					dlg.txtBoxCity.Text,
					dlg.txtBoxState.Text,
					dlg.txtBoxZipCode.Text,
					dlg.chkBxContracted.Checked);
			};
			dlg.Show();
		}

		private void exportToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog s = new SaveFileDialog
			{
				DefaultExt = "txt",
				Filter = "Text files (*.txt)|*.txt|All files(*.*)|*.*",
				FileName = "authors.txt"
			};

			s.FileOk += delegate(object sender1, CancelEventArgs e1)
			{
				List<string> lines = this.Inter.ExportAll();
				DataAccessTXT.DataWriter.WriteData(lines, s.FileName);
			};

			s.ShowDialog();
		}

		private void lvMain_MouseClick(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				if(lvMain.FocusedItem.Bounds.Contains(e.Location) == true)
				{
					contextMenuStripListView.Show(Cursor.Position);
				}
			}
		}

		private void removeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult r = MessageBox.Show("Are you sure you want to delete this item?", "Confirm Delete", MessageBoxButtons.OKCancel);
			if(r == DialogResult.OK)
			{
				string id = this.lvMain.FocusedItem.SubItems[0].Text;
				this.Inter.RemoveById(id);
				this.lvMain.Items.RemoveAt(this.lvMain.FocusedItem.Index);
				for(int i = 0; i < this.tvMain.Nodes.Count; i++)
				{
					if(this.tvMain.Nodes[i].Nodes[0].Text == id)
					{
						this.tvMain.Nodes[i].Remove();
					}
				}
			}
		}

		private void tvMain_MouseClick(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				if(this.tvMain.SelectedNode.Level == 0)
				{
					contextMenuStripTreeView.Show(Cursor.Position);
				}
			}
		}

		private void removeToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			DialogResult r = MessageBox.Show("Are you sure you want to delete this item?", "Confirm Delete", MessageBoxButtons.OKCancel);
			if(r == DialogResult.OK)
			{
				string id = this.tvMain.SelectedNode.Nodes[0].Text;
				this.Inter.RemoveById(id);
				for(int i = 0; i < this.lvMain.Items.Count; i++)
				{
					if(this.lvMain.Items[i].SubItems[0].Text == id)
					{
						this.lvMain.Items[i].Remove();
					}
				}
				this.tvMain.SelectedNode.Remove();
			}
		}
	}
}
