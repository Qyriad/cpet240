﻿// File:	Author.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

namespace Qyriad.Lab05.DataModel
{
	public struct Author
	{
		public string Id { get; private set; }
		public string LastName { get; private set; }
		public string FirstName { get; private set; }
		public string PhoneNumber { get; private set; }
		public string StreetAddress { get; private set; }
		public string City { get; private set; }
		public string State { get; private set; }
		public string ZipCode { get; private set; }
		public bool IsContracted { get; private set; }

		internal Author(string id, string lastName, string firstName, string phoneNumber, string streetAddress, string city,
			string state, string zipCode, bool isContracted)
		{
			this.Id = id;
			this.LastName = lastName;
			this.FirstName = firstName;
			this.PhoneNumber = phoneNumber;
			this.StreetAddress = streetAddress;
			this.City = city;
			this.State = state;
			this.ZipCode = zipCode;
			this.IsContracted = isContracted;
		}

		public string[] ToStringArray()
		{
			string[] s = new string[9];
			s[0] = this.Id;
			s[1] = this.LastName;
			s[2] = this.FirstName;
			s[3] = this.PhoneNumber;
			s[4] = this.StreetAddress;
			s[5] = this.City;
			s[6] = this.State;
			s[7] = this.ZipCode;
			s[8] = this.IsContracted.ToString();

			return s;
		}
	}
}
