﻿// File:	Extensions.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System.Collections.Generic;

namespace Qyriad.Lab05.DataModel
{
	public static class Extensions
	{
		public static string[] SplitByLength(this string str, int[] length)
		{
			int start = 0;
			List<string> s = new List<string>();
			foreach(int l in length)
			{
				if(start + l > str.Length)
				{
					s.Add(str.Substring(start));
				}
				else
				{
					s.Add(str.Substring(start, l).TrimEnd());
				}
				start += l + 1;
			}

			return s.ToArray();
		}
	}
}
