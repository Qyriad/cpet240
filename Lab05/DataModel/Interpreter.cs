﻿// File:	Interpeter.cs
// Author:	Mikaela RJ Szekely
// Project: NHTI CPET 240/Lab05
// Date:	10/06/2017
// License: MIT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Qyriad.Lab05.DataModel
{
    public struct Interpreter
    {
		private delegate string SpaceWrite(int n);
		public struct Field
		{
			public string Header;
			public int CharacterWidth;
		}

		public List<Field> Fields { get; private set; }
		public List<Author> Authors { get; private set; }

		public Interpreter(string[] dataLines)
		{
			// First line has the headers
			string headersLine = dataLines[0];

			// Second line has the dashes, which tell us the sizes of each field
			string dashesLine = dataLines[1];

			// Get the how many dashes per field
			string[] dashes = dashesLine.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
			int[] dashLengths = (from string s in dashes select s.Length).ToArray();

			// Now get each field label
			string[] headers = headersLine.SplitByLength(dashLengths);

			List<Field> cols = new List<Field>();

			for(int i = 0; i < dashLengths.Length; i++)
			{
				Field c = new Field
				{
					CharacterWidth = dashLengths[i],
					Header = headers[i]
				};
				cols.Add(c);
			}

			this.Fields = cols;

			this.Authors = new List<Author>(); 

			for(int i = 0; i < dataLines.Length - 2; i++) // - 2 to account for the headers and dashes lines
			{
				string[] items = dataLines[i + 2].SplitByLength(dashLengths); // + 2 to account for the headers and dashes lines
				bool contract;
				if(items[items.Length - 1] == "1")
				{
					contract = true;
				}
				else
				{
					contract = false;
				}

				this.Authors.Add(new Author(items[0], items[1], items[2], items[3], items[4], items[5], items[6], items[7], contract));
			}
		}

		private bool IsAnyNull(params object[] objs)
		{
			foreach(object o in objs)
			{
				if(o == null)
				{
					return true;
				}
			}

			return false;
		}

		public Interpreter(XmlNode rootNode)
		{
			this.Authors = new List<Author>();
			this.Fields = new List<Field>();

			Field first = new Field();

			first.Header = rootNode.FirstChild.Attributes[0].Name;

			foreach(XmlNode field in rootNode.FirstChild)
			{
				Field f = new Field();
				if(field.ChildNodes.Count > 1)
				{
					foreach(XmlNode ci in field.ChildNodes)
					{
						f.Header = ci.Name;
						this.Fields.Add(f);
					}
				}
				else
				{
					f.Header = field.Name;
					this.Fields.Add(f);
				}
			}

			
			foreach(XmlNode node in rootNode.ChildNodes)
			{
				Field f = new Field();
				f.Header = node.Attributes[0].Name;
				string id = node.Attributes.GetNamedItem(f.Header).Value;
				f = new Field();
				string lname = node.SelectSingleNode("au_lname").InnerText;
				string fname = node.SelectSingleNode("au_fname").InnerText;
				XmlNode contact = node.SelectSingleNode("contact");
				string phone = contact.SelectSingleNode("phone").InnerText;
				string stAddress = contact.SelectSingleNode("address").InnerText;
				string city = contact.SelectSingleNode("city").InnerText;
				string state = contact.SelectSingleNode("state").InnerText;
				string zip = contact.SelectSingleNode("phone").InnerText;
				bool contract = Convert.ToBoolean(int.Parse(node.SelectSingleNode("contract").InnerText));

				Author a = new Author(id, lname, fname, phone, stAddress, city, state, zip, contract);
				this.Authors.Add(a);
			}
		}

		public void AddNew(string id, string lastName, string firstName, string phoneNumber, string streetAddress, string city,
			string state, string zipCode, bool isContracted)
		{
			Author a = new Author(id, lastName, firstName, phoneNumber, streetAddress, city, state, zipCode, isContracted);
			this.Authors.Add(a);
		}

		public void RemoveById(string id)
		{
			for(int i = 0; i < this.Authors.Count; i++)
			{
				if(this.Authors[i].Id == id)
				{
					this.Authors.RemoveAt(i);
				}
			}
		}

		public List<string> ExportAll()
		{
			List<string> lines = new List<string>();

			string fields = "";
			string dashes = "";

			foreach(Field f in this.Fields)
			{
				fields += f.Header;

				for(int i = f.Header.Length; i < f.CharacterWidth; i++) // Add necessary whitespace
				{
					fields += " ";
				}

				for(int i = 0; i < f.CharacterWidth; i++)
				{
					dashes += "-";
				}
				fields += " ";
				dashes += " ";
			}

			lines.Add(fields);
			lines.Add(dashes);

			SpaceWrite writeSpaces = delegate(int n)
			{
				string spaces = "";
				for(int i = 0; i < n; i++)
				{
					spaces += " ";
				}
				return spaces;
			};


			for(int i = 0; i < this.Authors.Count; i++)
			{
				Author a = this.Authors[i];

				string line = "";
				line += a.Id;
				line += writeSpaces(this.Fields[0].CharacterWidth - a.Id.Length + 1); // + 1 to account for the extra single space between fields
				line += a.LastName;
				line += writeSpaces(this.Fields[1].CharacterWidth - a.LastName.Length + 1);
				line += a.FirstName;
				line += writeSpaces(this.Fields[2].CharacterWidth - a.FirstName.Length + 1);
				line += a.PhoneNumber;
				line += writeSpaces(this.Fields[3].CharacterWidth - a.PhoneNumber.Length + 1);
				line += a.StreetAddress;
				line += writeSpaces(this.Fields[4].CharacterWidth - a.StreetAddress.Length + 1);
				line += a.City;
				line += writeSpaces(this.Fields[5].CharacterWidth - a.City.Length + 1);
				line += a.State;
				line += writeSpaces(this.Fields[6].CharacterWidth - a.State.Length + 1);
				line += a.ZipCode;
				line += writeSpaces(this.Fields[7].CharacterWidth - a.ZipCode.Length + 1);
				line += (a.IsContracted ? ("1") : ("0"));

				lines.Add(line);
			}

			return lines;
		}
	}
}
