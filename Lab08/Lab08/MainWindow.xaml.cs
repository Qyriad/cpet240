﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Lab08
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Grid_Loaded(object sender, RoutedEventArgs e)
		{
			string sqlServer = "L231-07";
			// Read from SQL
			List<List<object>> rows = new List<List<object>>();
			using(SqlConnection connection = new SqlConnection($"Data Source={sqlServer};Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				using(SqlCommand cmd = new SqlCommand($"select * from authors", connection))
				{
					using(SqlDataReader reader = cmd.ExecuteReader())
					{
						reader.Read();
						List<object> headers = new List<object>();
						for(int i = 0; i < reader.FieldCount; i++)
						{
							headers.Add(reader.GetName(i));
						}

						rows.Add(headers);
						
						while(reader.Read())
						{
							object[] outArr = new object[reader.FieldCount];
							reader.GetValues(outArr);
							rows.Add(outArr.ToList());
						}
					}
				}
			}

			int index = 1;
			//foreach(List<object> row in rows)
			//{
			//	mainGrid.RowDefinitions.Add(new RowDefinition());
			//	Label l = new Label();
			//	l.Content = new TextBlock{ Text = "blank" };
			//	Grid.SetRow(l, index);
			//	mainGrid.Children.Add(l);
			//	index++;
			//}
			// Can I make each row the same?

			RowDefinition rd = new RowDefinition();
			rd.Height = new GridLength(20, GridUnitType.Pixel);
			mainGrid.RowDefinitions.Add(rd);
			rd = new RowDefinition();
			rd.Height = new GridLength(20, GridUnitType.Pixel);
			mainGrid.RowDefinitions.Add(rd);
			Label l = new Label();
			l.Content = "Row 0";
			mainGrid.Children.Add(l);
			Grid.SetRow(l, 0);
			l = new Label();
			l.Content = "Row 1";
			mainGrid.Children.Add(l);
			Grid.SetRow(l, 1);
		}
	}
}
