// File:	Qyriad.hpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab02
// Date:	09/12/2017
// License:	MIT

#pragma once

#include <Windows.h>
#include <cstdarg>
#include <cstdio>

#include "ServiceController.hpp"

namespace Qyriad
{
	extern bool UsingConsole;
	extern ServiceController *SC;

	int wprintf_s(wchar_t const *format, ...);
	int printf_s(char const *format, ...);
}
