// File:	WinMain.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab02
// Date:	09/12/2017
// License:	MIT

#include "ServiceController.hpp"


namespace Qyriad
{
	ServiceController::ServiceController(wchar_t const *serviceName) 
	{
		this->ServiceControlManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if(this->ServiceControlManager == INVALID_HANDLE_VALUE)
		{
			char buffer[80];
			sprintf_s(buffer, "Failed to create a handle to the service control manager; error %d\n", GetLastError());
			throw buffer;
		}

		this->Service = OpenService(this->ServiceControlManager, serviceName, SERVICE_ALL_ACCESS);
		OpenService(NULL, L"MSG", NULL);
		if(this->Service == NULL)
		{
			char buffer[80];
			sprintf_s(buffer, "Failed to create a handle to the service; error %d\n", GetLastError());
			throw buffer;
		}
	}

	ServiceController::~ServiceController()
	{
		CloseServiceHandle(this->Service);
		CloseServiceHandle(this->ServiceControlManager);
	}

	BOOL ServiceController::GetStatus(std::wstring *outStatus)
	{
		SERVICE_STATUS status;
		BOOL ret = QueryServiceStatus(this->Service, &status);
		*outStatus = ServiceStatusToWString(status.dwCurrentState);
		return ret;
	}

	BOOL ServiceController::GetStatus(DWORD *dwCurrentState)
	{
		SERVICE_STATUS status;
		BOOL ret = QueryServiceStatus(this->Service, &status);
		*dwCurrentState = status.dwCurrentState;
		return ret;
	}

	BOOL ServiceController::GetStatus(std::wstring *outStatus, DWORD *dwCurrentState)
	{
		SERVICE_STATUS status;
		BOOL ret = QueryServiceStatus(this->Service, &status);
		*outStatus = ServiceStatusToWString(status.dwCurrentState);
		*dwCurrentState = status.dwCurrentState;
		return ret;
	}

	BOOL ServiceController::Start()
	{
		return StartService(this->Service, 0, NULL);
	}

	BOOL ServiceController::Stop(SERVICE_STATUS *status)
	{

		return ControlService(this->Service, SERVICE_CONTROL_STOP, status);
	}

	BOOL ServiceController::Pause(SERVICE_STATUS *status)
	{
		return ControlService(this->Service, SERVICE_CONTROL_PAUSE, status);
	}

	BOOL ServiceController::Continue(SERVICE_STATUS *status)
	{
		return ControlService(this->Service, SERVICE_CONTROL_CONTINUE, status);
	}
}