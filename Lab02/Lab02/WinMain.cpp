// File:	WinMain.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab02
// Date:	09/12/2017
// License:	MIT

#include <Windows.h>
#include <iostream>
#include <cstdio>
#include <string>

#include "resource.h"
#include "Qyriad.hpp"
#include "ServiceController.hpp"

namespace ServiceWriter // Namespace the vars in Service Writer
{
	#include "serviceWriterShared.h"
}


INT_PTR CALLBACK DialogFunc(HWND hwDialog, UINT uMsg, WPARAM wParam, LPARAM lParam);
void ActivateConsole();
void DeactivateConsole();


int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, LPWSTR lpCmdLine, int nCmdShow)
{
	using Qyriad::printf_s;
	using Qyriad::wprintf_s;

	// Alright let's get a console if we have one
	if(AttachConsole(ATTACH_PARENT_PROCESS))
	{
		ActivateConsole();
	}
	else if(std::wstring(lpCmdLine).find(L"-v") != std::wstring::npos) // Or if -v is passed
	{
		AllocConsole();
		ActivateConsole();
	}
	
	HWND hwDialog = CreateDialogParam(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DialogFunc, NULL);
	if(hwDialog == NULL)
	{
		wprintf_s(L"Window creation failed; error %d\n", GetLastError());
		return 1;
	}

	ShowWindow(hwDialog, nCmdShow);

	MSG msg;
	BOOL ret;

	while((ret = GetMessage(&msg, NULL, 0, 0)) != FALSE)
	{
		if(ret == -1)
		{
			// Error!
			wprintf_s(L"Window message error %d\n", GetLastError());
			return 2;
		}
		if(!IsDialogMessage(hwDialog, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	if(Qyriad::UsingConsole) DeactivateConsole();

	if(Qyriad::SC != nullptr) delete Qyriad::SC;
	
	return 0;
}


INT_PTR CALLBACK DialogFunc(HWND hwDialog, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	using Qyriad::printf_s;
	using Qyriad::wprintf_s;

	static bool updateButtons = false; // Keep even when it goes out of scope
	if(updateButtons)
	{
		DWORD CurrentState;
		Qyriad::SC->GetStatus(&CurrentState);
		switch(CurrentState)
		{
			case SERVICE_STOPPED:
			{
				// Disable pause/continue/stop
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_PAUSE), FALSE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_CONTINUE), FALSE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_STOP), FALSE);
				// Enable start
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_START), TRUE);
				break;
			}
			case SERVICE_RUNNING:
			{
				// Disable start/continue
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_START), FALSE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_CONTINUE), FALSE);
				// Enable stop/pause
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_STOP), TRUE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_PAUSE), TRUE);
				break;
			}
			case SERVICE_PAUSED:
			{
				// Disable start/pause
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_START), FALSE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_PAUSE), FALSE);
				// Enable stop/continue
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_STOP), TRUE);
				EnableWindow(GetDlgItem(hwDialog, IDC_BUTTON_CONTINUE), TRUE);
				break;
			}
		}
		updateButtons = false;
	}

	switch(uMsg)
	{
		case WM_INITDIALOG:
		{
			try
			{
				Qyriad::SC = new Qyriad::ServiceController(L"MsgSender");
			}
			catch(char const *e)
			{
				MessageBoxA(NULL, e, NULL, MB_OK);
				return -1;
			}

			SetTimer(hwDialog, NULL, 1000, NULL);
			updateButtons = true;
			break;
		}
		case WM_COMMAND:
		{
			if(HIWORD(wParam) == 0)
			{
				switch(LOWORD(wParam)) // Switch menu ID
				{
					case IDC_BUTTON_START:
						wprintf_s(L"Start button pressed\n");
						// Start the service
						if(!Qyriad::SC->Start())
						{
							wprintf_s(L"Error starting service; error %d\n", GetLastError());
						}
						updateButtons = true;
						break;
					case IDC_BUTTON_STOP:
					{
						wprintf_s(L"Stop button pressed\n");
						SERVICE_STATUS status;
						if(!Qyriad::SC->Stop(&status))
						{
							wprintf_s(L"Error stopping service; error %d\n", GetLastError());
						}
						SetDlgItemText(hwDialog, IDC_STATIC_STATUS, Qyriad::SC->ServiceStatusToWString(status.dwCurrentState).c_str());
						updateButtons = true;
						break;
					}
					case IDC_BUTTON_PAUSE:
					{
						wprintf_s(L"Pause button pressed\n");
						SERVICE_STATUS status;
						if(!Qyriad::SC->Pause(&status))
						{
							wprintf_s(L"Error pausing service; error %d\n", GetLastError());
						}
						SetDlgItemText(hwDialog, IDC_STATIC_STATUS, Qyriad::SC->ServiceStatusToWString(status.dwCurrentState).c_str());
						updateButtons = true;
						break;
					}
					case IDC_BUTTON_CONTINUE:
					{
						wprintf_s(L"Continue button pressed\n");
						SERVICE_STATUS status;
						if(!Qyriad::SC->Continue(&status))
						{
							wprintf_s(L"Error resuming service; error %d\n", GetLastError());
						}
						updateButtons = true;
						break;
					}
				}
				return TRUE;
			}
			else
			{
				return TRUE;
			}

			break;
		}
		case WM_TIMER:
		{
			std::wstring out;
			DWORD currentState;
			if(Qyriad::SC->GetStatus(&out, &currentState))
			{
				wprintf_s(L"Status: %s\n", out.c_str());
				SetDlgItemText(hwDialog, IDC_STATIC_STATUS, out.c_str());
			}
			else
			{
				wprintf_s(L"Error querying service; error %d\n", GetLastError());
			}
			updateButtons = true;

			if(currentState == SERVICE_RUNNING)
			{
				// Open the file
				HANDLE hFile = CreateFile(L"C:/zing/time.txt", FILE_GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
				if(hFile == INVALID_HANDLE_VALUE)
				{
					wprintf_s(L"Error creating file handle; error %d\n", GetLastError());
					return TRUE;
				}

				char buffer[80];
				memset(buffer, 0, 80);
				DWORD byteCount;
				if(!ReadFile(hFile, buffer, 80, &byteCount, NULL))
				{
					wprintf_s(L"Error reading file; error %d\n", GetLastError());
					CloseHandle(hFile);
					return TRUE;
				}

				CloseHandle(hFile);
				DeleteFile(L"C:/zing/time.txt");

				SetDlgItemTextA(hwDialog, IDC_STATIC_CONTENTS, buffer);
			}

			return TRUE;
			break;
		}
		case WM_CLOSE:
			DestroyWindow(hwDialog);
			return TRUE;
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			return TRUE;
			break;
	}

	return FALSE;
}


inline void ActivateConsole()
{
	FILE *fp;
	_wfreopen_s(&fp, L"CONOUT$", L"w", stdout); // Open CONOUT$ to fp and stdout, though we don't care about fp
	Qyriad::UsingConsole = true;
	wprintf_s(L"\nConsole mode active\n");
}


inline void DeactivateConsole()
{
	INPUT ip;
	// Set up generic keyboard event.
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0; // hardware scan code for key
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Send the "Enter" key
	ip.ki.wVk = 0x0D; // virtual-key code for the "Enter" key
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Enter" key
	ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
	SendInput(1, &ip, sizeof(INPUT));

	FreeConsole();
}