// File:	WinMain.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab02
// Date:	09/12/2017
// License:	MIT

#pragma once

#include <string>
#include <Windows.h>

namespace Qyriad { class ServiceController;  }

class Qyriad::ServiceController
{
	private:
		SC_HANDLE ServiceControlManager;
		SC_HANDLE Service;
	public:
		ServiceController(wchar_t const *serviceName);
		~ServiceController();

		BOOL GetStatus(std::wstring *outStatus);
		BOOL GetStatus(DWORD *dwCurrentState);
		BOOL GetStatus(std::wstring *outStatus, DWORD *dwCurrentState);
		BOOL Start();
		BOOL Stop(SERVICE_STATUS *status);
		BOOL Pause(SERVICE_STATUS *status);
		BOOL Continue(SERVICE_STATUS *status);

		static std::wstring ServiceStatusToWString(DWORD status)
		{
			std::wstring s;
			switch(status)
			{
				case SERVICE_STOPPED:
					s = L"Stopped";
					break;
				case SERVICE_START_PENDING:
					s = L"Start Pending";
					break;
				case SERVICE_STOP_PENDING:
					s = L"Stop Pending";
					break;
				case SERVICE_RUNNING:
					s = L"Running";
					break;
				case SERVICE_CONTINUE_PENDING:
					s = L"Continue Pending";
					break;
				case SERVICE_PAUSE_PENDING:
					s = L"Pause Pending";
					break;
				case SERVICE_PAUSED:
					s = L"Paused";
					break;
				default:
					s = L"Unknown";
					break;
			}
			return s;
		}
};