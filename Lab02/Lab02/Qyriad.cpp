// File:	Qyriad.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab02
// Date:	09/12/2017
// License:	MIT

#include "Qyriad.hpp"


namespace Qyriad
{
	bool UsingConsole{false};
	ServiceController *SC{nullptr};

	int wprintf_s(wchar_t const *format, ...)
	{
		if(!UsingConsole)
		{
			return 0;
		}
		va_list args;
		va_start(args, format);
		return vwprintf_s(format, args);
	}

	int printf_s(char const *format, ...)
	{
		if(!UsingConsole)
		{
			return 0;
		}
		va_list args;
		va_start(args, format);
		return vprintf_s(format, args);
	}
}