#ifndef UNICODE
#define UNICODE
#endif

#include <cstdio>
#include <cwchar>
#include <functional>

#include <Windows.h>
#include <CommCtrl.h>
#include <sql.h>
#include <strsafe.h>

#include "resource.h"

#include "..\Native\Native.h"

#pragma region forward_declarations
void InitListViewColumns(HWND hWndListView, std::vector<Native::SqlObj> column_headers);
void AddRowToListView(HWND hWndListView, std::vector<Native::SqlObj> row);
// standard string to wstring
void sstosws(std::string * const str, std::wstring *wstr);
#pragma endregion forward_delcarations

INT_PTR CALLBACK WndMainProc(_In_ HWND hwnd, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam);

std::vector<std::vector<Native::SqlObj>> *sql_rows;

HWND hWndListViewMain;


INT WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	HWND hWndMain = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG_MAIN), NULL, &WndMainProc);

	auto rows = Native::GetRows("pubs", "stores");
	sql_rows = &rows;

	HWND hWndListView = GetDlgItem(hWndMain, IDC_LIST1);
	hWndListViewMain = hWndListView;

	ListView_SetExtendedListViewStyle(hWndListView, LVS_EX_FULLROWSELECT);

	InitListViewColumns(hWndListView, sql_rows->at(0));

	for(int i = 1; i < sql_rows->size(); i++)
	{
		AddRowToListView(hWndListView, sql_rows->at(i));
	}

	ListView_SetColumnWidth(hWndListView, sql_rows->at(0).size() - 1, LVSCW_AUTOSIZE_USEHEADER); // For some reason I have to do this with the zip field

	ShowWindow(hWndMain, nCmdShow);

	MSG msg;
	BOOL ret;

	while((ret = GetMessage(&msg, NULL, 0, 0)) != FALSE)
	{
		if(ret == -1)
		{
			// Error!
			wprintf_s(L"Window message error %d\n", GetLastError());
			return 2;
		}
		if(!IsDialogMessage(hWndMain, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return 0;
}

INT_PTR CALLBACK WndOrderProc(_In_ HWND hWndOrder, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_CLOSE:
			DestroyWindow(hWndOrder);
			return TRUE;
			break;

		case WM_DESTROY:
			//PostQuitMessage(0);
			return TRUE;
	}
	return FALSE;
}

INT_PTR CALLBACK WndMainProc(_In_ HWND hWndMain, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_NOTIFY:
		{
			NMHDR *notice = reinterpret_cast<NMHDR*>(lParam);
			HWND hWndfrom = notice->hwndFrom;
			UINT_PTR idFrom = notice->idFrom;
			UINT code = notice->code;
			if(idFrom == IDC_LIST1)
			{
				if(code == NM_DBLCLK)
				{
					NMITEMACTIVATE *info = reinterpret_cast<NMITEMACTIVATE*>(lParam);
					int iitem = info->iItem;
					if(iitem == -1) return TRUE;
					int isub = info->iSubItem;
					//WCHAR wstr[512];
					//StringCchPrintfW(wstr, 512, L"Item %d subitem %d", iitem, isub);
					//MessageBox(NULL, wstr, L"Ordering from store", NULL);
					HWND hOrderDialog = CreateDialog(NULL, MAKEINTRESOURCE(IDD_DIALOG_ORDER), NULL, &WndOrderProc);
					ShowWindow(hOrderDialog, SW_SHOW);
				}
			}
		}

		case WM_SIZE:
		{
			return FALSE;
		}

		case WM_SIZING:
		{
			RECT *newloc = reinterpret_cast<RECT*>(lParam);
			int neww = newloc->right - newloc->left;
			int newh = newloc->bottom - newloc->top;
			MoveWindow(hWndListViewMain, 7, 7, neww - 35, newh - 55, TRUE);
			return TRUE;
		}

		case WM_CLOSE:
			DestroyWindow(hWndMain);
			return TRUE;
			break;
			
		case WM_DESTROY:
			PostQuitMessage(0);
			return TRUE;
			break;
	}

	return FALSE;
}

void InitListViewColumns(HWND hWndListView, std::vector<Native::SqlObj> column_headers)
{
	// Init lv columns
	WCHAR szText[256];
	LVCOLUMN lvc;

	// Init lvc

	// Mask specifies format width text and subitem members of the struct are valid
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	for(int icol = 0; icol < sql_rows[0].size(); icol++)
	{
		lvc.iSubItem = icol;
		std::string s = (*sql_rows)[0][icol].str();
		std::wstring ws;
		sstosws(&s, &ws);
		WCHAR *wstr = new WCHAR[ws.length() + 1]; 
		StringCchCopy(wstr, ws.length() + 1, ws.c_str());

		lvc.pszText = wstr;

		int width = ListView_GetStringWidth(hWndListView, wstr);

		lvc.cx = width + 20; // ~20 seems to be the padding size
		lvc.fmt = LVCFMT_LEFT;

		// Insert the columns into the list view
		ListView_InsertColumn(hWndListView, icol, &lvc);

		delete wstr;
	}
}

void AddRowToListView(HWND hWndListView, std::vector<Native::SqlObj> row)
{
	int current_width = ListView_GetColumnWidth(hWndListView, 0);

	LVITEM lvi;
	std::string s = row.at(0).str();
	std::wstring ws;
	sstosws(&s, &ws);
	WCHAR *wstr = new WCHAR[ws.length() + 1];
	StringCchCopy(wstr, ws.length() + 1, ws.c_str());
	lvi.pszText = wstr;
	lvi.mask = LVIF_TEXT | LVIF_STATE;
	lvi.stateMask = 0;
	lvi.iSubItem = 0;
	lvi.state = 0;
	lvi.iItem = 0;
	ListView_InsertItem(hWndListView, &lvi);

	int alt_width = ListView_GetStringWidth(hWndListView, wstr);
	if(current_width < alt_width)
	{
		ListView_SetColumnWidth(hWndListView, 0, alt_width);
	}

	delete wstr;

	for(int i = 1; i < row.size(); i++)
	{
		current_width = ListView_GetColumnWidth(hWndListView, i);

		lvi.iSubItem = i;
		s = row.at(i).str();
		sstosws(&s, &ws);
		wstr = new WCHAR[ws.length() + 1];
		StringCchCopy(wstr, ws.length() + 1, ws.c_str());
		lvi.pszText = wstr;
		ListView_SetItem(hWndListView, &lvi);

		alt_width = ListView_GetStringWidth(hWndListView, wstr);
		if(current_width < alt_width)
		{ 
			ListView_SetColumnWidth(hWndListView, i, alt_width + 20);
		}

		delete wstr;
	}
}

void sstosws(std::string * const str, std::wstring *wstr)
{
	std::string &s = *str;
	std::wstring &ws = *wstr;

	ws = std::wstring(s.begin(), s.end());
}