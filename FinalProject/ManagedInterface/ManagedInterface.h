// ManagedInterface.h

#pragma once

//#include <tuple>
//#include <vector>
//#include <msclr/marshal.h>
//#include <msclr/marshal_windows.h>
//#include <msclr/marshal_cppstd.h>
#include <vector>
using namespace System;
using namespace System::Data::SqlClient;
using namespace System::Collections::Generic;
//using namespace msclr::interop;


namespace Managed
{

	public ref class DataAccess
	{
		public:
			static List<List<Object^>^>^ GetAllRows(String^ database, String^ table);
	};

	enum SqlObjType
	{
		Str,
		Int,
		Bool,
		Uninitialized
	};
}