// This is the main DLL file.

#include "stdafx.h"

#include "ManagedInterface.h"

//using namespace Managed;
using namespace System::Data::SqlClient;


List<List<Object^>^>^ Managed::DataAccess::GetAllRows(String^ database, String^ table)
{
	List<List<Object^>^>^ rows = gcnew List<List<Object^>^>();

	IO::StreamReader^ file = IO::File::OpenText("connectionstring.txt");
	String^ connection_string = file->ReadLine();
	file->Close();
	
	SqlConnection connection(connection_string);

	connection.Open();
	connection.ChangeDatabase(database);

	SqlCommand cmd("select * from " + table, %connection);
	
	SqlDataReader^ reader = cmd.ExecuteReader();

	// First get the headers
	reader->Read();
	List<Object^> headers;
	for(int i = 0; i < reader->FieldCount; i++)
	{
		headers.Add(reader->GetName(i));
	}
	rows->Add(%headers);

	// Now get the rest of the rows
	while(reader->Read())
	{
		array<Object^>^ a = gcnew array<Object^>(reader->FieldCount);
		reader->GetValues(a);
		List<Object^> l(reader->FieldCount);
		for(int i = 0; i < a->Length; i++)
		{
			l.Add(a[i]);
		}
		rows->Add(%l);
	}

	//cmd.Dispose();

	connection.Close();

	return rows;
}


/*
std::vector<std::vector<SqlObj>> *ManagedInterface::DataAccess::GetAllRows(std::string database, std::string table)
{
	std::vector<std::vector<SqlObj>> *rows;
	
	SqlConnection connection("Data Source=L231-10;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
	connection.ChangeDatabase(marshal_as<String^>(database));

	SqlCommand cmd("select * from " + marshal_as<String^>(table), %connection);

	SqlDataReader^ reader = cmd.ExecuteReader();
	
	// First get the headers
	reader->Read();
	std::vector<SqlObj> headers;
	for(int i = 0; i < reader->FieldCount; i++)
	{
		String^ c = reader->GetName(i);
		//marshal_context m;
		std::string str = marshal_as<std::string>(c);
		//SqlObj s(str);
		//headers.push_back(s); // PROBLEM
	}

	//rows->push_back(headers); // PROBLEM

	// Now get the rest of the rows
	while(reader->Read())
	{
		array<Object^>^ a = gcnew array<Object^>(reader->FieldCount);
		reader->GetValues(a);
		//std::vector<SqlObj> b(a->Length); // PROBLEM

		for(int i = 0; i < a->Length; i++)
		{
			Type^ t = a[i]->GetType();
			if(t == String::typeid)
			{
				String^ s = safe_cast<String^>(a[i]);
				std::string str = marshal_as<std::string>(s);
			}
		}
		//rows->push_back(b);
		
	}

	connection.Close();

	return rows;
}
*/
