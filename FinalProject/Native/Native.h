// Native.h

#pragma once
#pragma unmanaged

#include <vector>

//using namespace System;

namespace Native 
{
	enum SqlObjType
	{
		Int,
		Bool,
		Str,
		Float,
		Uninitialized
	};

	class SqlObj
	{
		private:
			void *data;
			SqlObjType data_type;

		public:
			SqlObjType __declspec(dllexport) type();
			std::string __declspec(dllexport) str();

			SqlObj() : data(nullptr), data_type(SqlObjType::Uninitialized) {}

			SqlObj(void *d, SqlObjType t) : data(d), data_type(t) {}
	};

	std::vector<std::vector<SqlObj>> __declspec(dllexport) GetRows(std::string database, std::string table);
}
