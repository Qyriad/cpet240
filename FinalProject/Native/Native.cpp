// This is the main DLL file.

#include "stdafx.h"

#include "Native.h"

#pragma managed
//#include "..\ManagedInterface\ManagedInterface.h"
#include <msclr\marshal.h>
#pragma unmanaged


Native::SqlObjType Native::SqlObj::type()
{
	return this->data_type;
}

std::string Native::SqlObj::str()
{
	if(this->data_type != SqlObjType::Str)
	{
		throw std::exception("Bad SqlObj access");
	}
	std::string ret((char * const)this->data);

	return ret;
}

#pragma managed
std::vector<std::vector<Native::SqlObj>> P_GetRows(std::string database, std::string table)
{
	using namespace System;
	using namespace System::Collections::Generic;
	using namespace msclr::interop;
	using namespace System::Runtime::InteropServices;


	std::vector<std::vector<Native::SqlObj>> rows;

	String^ db = gcnew String(database.c_str());
	String^ tb = gcnew String(table.c_str());


	List<List<Object^>^>^ mrows = Managed::DataAccess::GetAllRows(db, tb);

	for(int i = 0; i < mrows->Count; i++)
	{
		std::vector<Native::SqlObj> row;
		//List<Object^>^ mrow = mrows[i];
		for(int j = 0; j < mrows[i]->Count; j++)
		{
			Object^ mobj = mrows[i][j];
			//auto mobjs = mrows[i];
			if(mobj->GetType() == String::typeid)
			{
				void *cstr = (Marshal::StringToHGlobalAnsi((String^) mobj)).ToPointer();
				Native::SqlObj obj(cstr, Native::SqlObjType::Str);
				row.push_back(obj);
			}
			else if(mobj->GetType() == Single::typeid)
			{
				//float *f = new float(marshal_as<float>((Single)mobj));
				float *f = new float((float)mobj);
				Native::SqlObj obj((void*)f, Native::SqlObjType::Float);
				row.push_back(obj);
			}
			else if(mobj->GetType() == Int32::typeid)
			{
				int *i = new int((int)mobj);
				Native::SqlObj obj((void*)i, Native::SqlObjType::Int);
				row.push_back(obj);
			}
			else if(mobj->GetType() == Boolean::typeid)
			{
				bool *b = new bool((bool)mobj);
				Native::SqlObj obj((void*)b, Native::SqlObjType::Bool);
				row.push_back(obj);
			}
			else
			{
				throw std::exception("Help me (Native.cpp)");
			}
		}
		rows.push_back(row);
	}

	return rows;
}

#pragma unmanaged
std::vector<std::vector<Native::SqlObj>> Native::GetRows(std::string database, std::string table)
{
	//using namespace System;
	//using namespace msclr::interop;

	//std::vector<std::vector<Native::SqlObj>> rows;

	//String^ db = marshal_as<String^>(database);
	//String^ tb = marshal_as<String^>(table);

	//auto managed_rows = Managed::DataAccess::GetAllRows(db, tb);

	return P_GetRows(database, table);

	//return rows;
}