// ConsoleApplication2.cpp : main project file.

#include "stdafx.h"

//#include "..\Native\Native.h"
#pragma unmanaged

#include <vector>

#pragma managed
#include <iostream>
#include <string>

#pragma unmanaged

#include "..\Native\Native.h"

//using namespace System;

/*namespace Native
{

	enum SqlObjType
	{
		Int,
		Bool,
		Str,
		Uninitialized
	};

	class SqlObj
	{
		private:
			void *data;
			SqlObjType data_type;

		public:
			std::string __declspec(dllexport) str();
			SqlObjType __declspec(dllexport) type();
	};

	std::vector<std::vector<SqlObj>> __declspec(dllexport) GetRows(std::string database, std::string table);
}*/

#pragma managed

using namespace System;

int main(array<System::String ^> ^args)
{
    //Console::WriteLine("Hello World");

	Native::SqlObj a;
	a.type();

	auto b = Native::GetRows("pubs", "authors");

	std::string str = b.at(0).at(0).str();

	std::cout << str << std::endl;

    return 0;
}
