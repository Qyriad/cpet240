﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DataModel;

namespace Lab04
{
	public partial class MainForm : Form
	{
		private Interpreter Inter;
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			this.lvMain.View = View.Details;
			string[] lines = DataAccess.DataReader.ReadData();
			this.Inter = new Interpreter(lines);
			foreach(Interpreter.Field col in this.Inter.Fields)
			{
				this.lvMain.Columns.Add(col.Header);
			}

			foreach(Author a in this.Inter.Authors)
			{
				this.lvMain.Items.Add(new ListViewItem(a.ToStringArray()));
			}

			foreach(ColumnHeader c in this.lvMain.Columns)
			{
				c.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
			}

			foreach(Author a in this.Inter.Authors)
			{
				TreeNode n = new TreeNode($"{a.LastName}, {a.FirstName}");
				n.Nodes.Add(a.Id);
				TreeNode address = new TreeNode("Address");
				address.Nodes.Add(a.StreetAddress);
				address.Nodes.Add(a.City);
				address.Nodes.Add(a.State);
				address.Nodes.Add(a.ZipCode);
				n.Nodes.Add(address);
				n.Nodes.Add(a.PhoneNumber);
				n.Nodes.Add(a.IsContracted ? "Contracted" : "Not Contracted");

				this.tvMain.Nodes.Add(n);
			}
		}

		private void addToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewAuthorDialog dlg = new NewAuthorDialog();
			dlg.btnAdd.Click += delegate(object sender1, EventArgs e1)
			{
				string[] s =
				{
					dlg.txtBoxId.Text,
					dlg.txtBoxLastName.Text,
					dlg.txtBoxFirstName.Text,
					dlg.txtBoxPhoneNumber.Text,
					dlg.txtBoxAddress.Text,
					dlg.txtBoxCity.Text,
					dlg.txtBoxState.Text,
					dlg.txtBoxZipCode.Text,
					dlg.chkBxContracted.Checked.ToString()
				};

				this.lvMain.Items.Add(new ListViewItem(s));
				{
					TreeNode n = new TreeNode($"{dlg.txtBoxLastName.Text}, {dlg.txtBoxFirstName.Text}");
					n.Nodes.Add(dlg.txtBoxId.Text);
					TreeNode address = new TreeNode("Address");
					address.Nodes.Add(dlg.txtBoxAddress.Text);
					address.Nodes.Add(dlg.txtBoxCity.Text);
					address.Nodes.Add(dlg.txtBoxState.Text);
					address.Nodes.Add(dlg.txtBoxZipCode.Text);
					n.Nodes.Add(address);
					n.Nodes.Add(dlg.txtBoxPhoneNumber.Text);
					n.Nodes.Add(dlg.chkBxContracted.Checked ? "Contracted" : "Not Contracted");
					this.tvMain.Nodes.Add(n);
				}

				this.Inter.AddNew(dlg.txtBoxId.Text,
					dlg.txtBoxLastName.Text,
					dlg.txtBoxFirstName.Text,
					dlg.txtBoxPhoneNumber.Text,
					dlg.txtBoxAddress.Text,
					dlg.txtBoxCity.Text,
					dlg.txtBoxState.Text,
					dlg.txtBoxZipCode.Text,
					dlg.chkBxContracted.Checked);
			};
			dlg.Show();
		}

		private void exportToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog s = new SaveFileDialog
			{
				DefaultExt = "txt",
				Filter = "Text files (*.txt)|*.txt|All files(*.*)|*.*",
				FileName = "authors.txt"
			};

			s.FileOk += delegate (object sender1, CancelEventArgs e1)
			{
				List<string> lines = this.Inter.ExportAll();
				DataAccess.DataWriter.WriteData(lines, s.FileName);
			};

			s.ShowDialog();
		}
	}
}
