﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
	public static class Extensions
	{
		public static string[] SplitByLength(this string str, int[] length)
		{
			int start = 0;
			List<string> s = new List<string>();
			foreach(int l in length)
			{
				if(start + l > str.Length)
				{
					s.Add(str.Substring(start));
				}
				else
				{
					s.Add(str.Substring(start, l).TrimEnd());
				}
				start += l + 1;
			}

			return s.ToArray();
		}
	}
}
