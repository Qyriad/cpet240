﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DataReader
	{
		public static string[] ReadData()
		{
			string path = ConfigurationManager.AppSettings.Get("filepath");
			List<string> data = new List<string>();

			using (StreamReader sr = new StreamReader(path))
			{
				while(!sr.EndOfStream)
				{
					data.Add(sr.ReadLine());
				}
			}

			return data.ToArray();
		}
	}
}
