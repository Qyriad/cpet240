﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
	public class DataWriter
	{
		public static void WriteData(ICollection<string> lines, string file)
		{
			using(System.IO.StreamWriter sw = new System.IO.StreamWriter(file))
			{
				foreach(string line in lines)
				{
					sw.WriteLine(line);
				}
			}
		}
	}
}
