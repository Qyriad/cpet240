// File:	ZingTime.hpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab01
// Date:	09/07/2017
// License:	MIT

#pragma once

#include <string>
#include <Windows.h>

#include "resource.h"

namespace qyriad { class ZingTime;   }

class qyriad::ZingTime
{
	private:
		std::string filename;
	public:
		ZingTime(HWND hwDialog);

		std::string GetTime() const;
};