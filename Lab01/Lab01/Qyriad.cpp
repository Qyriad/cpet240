// File:	qyriad.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab01
// Date:	09/07/2017
// License:	MIT

#include "qyriad.hpp"

namespace qyriad
{

		bool usingConsole {false};
		LPCWSTR TimeFilePath {L"C:/zing/time.txt"};
		DWORD ThreadId {0};
		WCHAR UserEnteredPath[INT16_MAX];
		Mode_t Mode {STOP_MODE};
}

int qyriad::wprintf_s(const wchar_t *format, ...) // Checks if we're in console mode before printing to save resources (I profiled it, it's worth it)
{
	if(!usingConsole)
	{
		return 0;
	}
	va_list args;
	va_start(args, format);
	return vwprintf_s(format, args);
}

int qyriad::printf_s(const char *format, ...) // Checks if we're in console mode before printing to save resources (I profiled it, it's worth it)
{
	if(!usingConsole)
	{
		return 0;
	}
	va_list args;
	va_start(args, format);
	return vprintf_s(format, args);
}