//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Dialog.rc
//
#define IDD_DIALOG1                     101
#define IDC_EDIT_FILENAME               1001
#define IDC_STATIC_FILENAME             1002
#define IDC_STATIC_TIME_READ            1003
#define IDC_STATIC_TIME                 1004
#define IDC_BUTTON_START                1005
#define IDC_BUTTON_STOP                 1006
#define IDC_RADIO_TIMER_MODE            1008
#define IDC_RADIO_THREAD_MODE           1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
