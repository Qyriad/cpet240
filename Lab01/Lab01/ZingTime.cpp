// File:	ZingTime.cpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab01
// Date:	09/07/2017
// License:	MIT

#include "ZingTime.hpp"
#include "qyriad.hpp"



qyriad::ZingTime::ZingTime(HWND hwDialog)
{
	// Open the file
	wchar_t buffer[INT16_MAX];

	GetDlgItemText(hwDialog, IDC_EDIT_FILENAME, buffer, INT16_MAX);
	// Buffer has the filename
	wprintf_s(L"Using file %s...\n", buffer);
	wcscpy_s(UserEnteredPath, buffer);
}


std::string qyriad::ZingTime::GetTime() const
{
	using qyriad::printf_s;
	using qyriad::wprintf_s;

	HANDLE hFile = CreateFile(UserEnteredPath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		int err = GetLastError();
		switch(err)
		{
			case ERROR_FILE_NOT_FOUND:
				wprintf_s(L"File not found\n");
				break;
			case ERROR_SHARING_VIOLATION:
				wprintf_s(L"Cannot access the file because it is being used by another process\n");
				break;
			default:
				wprintf_s(L"Failed to create the file handle; error %d\n", err);
				break;
		}
		return ""; // Return early, no point in trying to read a file we can't get a handle on
	}
	DWORD byteCount;
	char buffer[UINT8_MAX];
	memset(buffer, 0, UINT8_MAX);
	BOOL res = ReadFile(
		hFile,
		buffer,
		UINT8_MAX,
		&byteCount,
		NULL);
	if(res != TRUE)
	{
		wprintf_s(L"File read error %d\n", GetLastError());
		return nullptr;
	}

	// Buffer should have the contents of the file
	printf_s("Read file with data: %s\n", buffer);

	CloseHandle(hFile);
	DeleteFile(UserEnteredPath);

	return std::string(buffer);
}