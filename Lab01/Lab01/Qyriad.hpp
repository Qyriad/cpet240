// File:	qyriad.hpp
// Author:	Mikaela RJ Szekely
// Project:	NHTI CPET 240/Lab01
// Date:	09/07/2017
// License:	MIT

#pragma once

#include <string>
#include <vector>
#include <cstdio>
#include <cstdarg>
#include <cstdint>

#include <Windows.h>


namespace qyriad
{
		enum Mode_t
		{
			STOP_MODE,
			TIMER_MODE,
			THREAD_MODE
		};

		extern bool usingConsole;
		extern LPCWSTR TimeFilePath;
		extern WCHAR UserEnteredPath[INT16_MAX];
		extern Mode_t Mode;

		int wprintf_s(const wchar_t *format, ...);
		int printf_s(const char *format, ...);


		template<typename T> // Because I like to make more work for myself, let's make a split function for any string
		std::vector<std::basic_string<T>> split_basic_string(std::basic_string<T> s, T delim)
		{
			std::vector<std::basic_string<T>> v;
			size_t sistart = 0, siend = s.find(delim);

			do
			{
				// Substring from si inclusive to delim index exclusive; ie, exclude the delim; substract length and index to get count
				v.push_back(s.substr(sistart, siend - sistart));
				sistart = siend + 1; // Again, don't include the delim
				siend = s.find(delim, sistart); // Get next delim index
			} while(siend != std::string::npos);

			// Add the last one
			v.push_back(s.substr(sistart));

			return v;
		}

		inline std::vector<std::string> split_string(std::string s, char delim)
		{
			return split_basic_string(s, delim);
		}
}