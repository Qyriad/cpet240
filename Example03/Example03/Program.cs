﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example03
{
	class Program
	{
		public delegate void delShowValue(string x);

		static void outputUpper(string s)
		{
			Console.WriteLine(s.ToUpper());
		}

		static void outputLower(string s)
		{
			Console.WriteLine(s.ToLower());
		}

		static void Main(string[] args)
		{
			delShowValue delObj;
			delObj = new delShowValue(outputLower);
			delObj += new delShowValue(outputUpper);
			delObj("HELLO world");


			try
			{
				int x = int.Parse("Hello world");
			}
			catch (FormatException e)
			{
				Console.WriteLine("Format exception: " + e.Message);
				Console.WriteLine(e.GetType().FullName);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				Console.WriteLine(e.GetType().FullName);
			}
			finally
			{
				Console.WriteLine("Finally");
			}
		}
	}
}
