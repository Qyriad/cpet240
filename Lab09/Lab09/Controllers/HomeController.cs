﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace Lab09.Controllers
{
	public class Author
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }

		public Author() { }
		public Author(string id, string name, int age)
		{
			Id = id;
			Name = name;
			Age = age;
		}
	}

	public class HomeController : Controller
    {
        public IActionResult Index()
        {
			List<Author> authors = GetAuthors();

            return View(authors);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
		
		private List<Author> GetAuthors()
		{
			List<Author> authors = new List<Author>();
			/*
			authors.Add(new Author("1", "Ken", 22));
			authors.Add(new Author("2", "Bill", 33));
			authors.Add(new Author("3", "Mary", 44));
			authors.Add(new Author("4", "Jon", 55));
			authors.Add(new Author("5", "Hank", 66));
			authors.Add(new Author("6", "Jill", 77));
			authors.Add(new Author("7", "Rusty", 88));
			authors.Add(new Author("8", "Kerry", 99));
			*/
			List<List<object>> objs = DataAccess.DataReader.ReadAllRowsFromTable("authors");
			AuthorRepository AuRepo = new AuthorRepository(objs.Skip(1).ToList(), from header in objs[0] where header is string select header as string);
			

			return authors;
		}
	}
}
