﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DataAccess
{
	public class DataReader
	{
		public const string CONNECTION_STRING = "Data Source=YUKI;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

		public static List<List<object>> ReadAllRowsFromTable(string table)
		{
			List<List<object>> rows = new List<List<object>>();
			using(SqlConnection connection = new SqlConnection(CONNECTION_STRING))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				using(SqlCommand cmd = new SqlCommand($"select * from {table}", connection))
				{
					using(SqlDataReader reader = cmd.ExecuteReader())
					{
						// First get the headers
						reader.Read();
						List<object> headers = new List<object>();
						for(int i = 0; i < reader.FieldCount; i++)
						{
							headers.Add(reader.GetName(i));
						}
						rows.Add(headers);

						while(reader.Read())
						{
							object[] outArr = new object[reader.FieldCount];
							reader.GetValues(outArr);
							rows.Add(outArr.ToList());
						}
					}
				}
			}
			return null;
		}
	}
}

