﻿using System;

namespace DataModel
{
    public class Author
    {
		public string Id { get; private set; }
		public string LastName { get; private set; }
		public string FirstName { get; private set; }
		public string PhoneNumber { get; private set; }
		public string StreetAddress { get; private set; }
		public string City { get; private set; }
		public string State { get; private set; }
		public string ZipCode { get; private set; }
		public bool IsContracted { get; private set; }
		
		public Author(string id, string lastName, string firstName, string phoneNumber, string streetAddress, string city, string state, string zipCode, bool isContracted)
		{
			this.Id = id;
			this.LastName = lastName;
			this.FirstName = firstName;
			this.PhoneNumber = phoneNumber;
			this.StreetAddress = streetAddress;
			this.City = city;
			this.State = state;
			this.ZipCode = zipCode;
			this.IsContracted = isContracted;
		}
    }
}
