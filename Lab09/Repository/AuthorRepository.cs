﻿using System;
using System.Collections.Generic;
using DataModel;

namespace Repository
{
    public class AuthorRepository
    {
		public List<Author> Authors { get; private set; }
		public List<string> Fields { get; private set; }

		public AuthorRepository(List<List<object>> sqlAuthorRows, IEnumerable<string> sqlAuthorHeaders)
		{
			this.Authors = new List<Author>();

			foreach(List<object> row in sqlAuthorRows)
			{
				string id = row[0].ToString();
				string lname = row[1].ToString();
				string fname = row[2].ToString();
				string phone = row[3].ToString();
				string addr = row[4].ToString();
				string city = row[5].ToString();
				string state = row[6].ToString();
				string zip = row[7].ToString();
				bool contract = (bool)row[8];

				Author a = new Author(id, lname, fname, phone, addr, city, state, zip, contract);
				this.Authors.Add(a);
			}
		}
    }
}
