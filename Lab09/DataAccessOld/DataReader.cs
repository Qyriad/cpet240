﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DataAccess
{
	public class DataReader
	{
		public const string CONNECTION_STRING = "";

		public static List<List<object>> ReadAllRowsFromTable(string table)
		{
			List<List<object>> rows = new List<List<object>>();
			using(SqlConnection connection = new SqlConnection(CONNECTION_STRING))
			{
				using(SqlCommand cmd = new SqlCommand($"select * from {table}", connection))
				{
					using(SqlDataReader reader = cmd.ExecuteReader())
					{
						// First get the headers
						reader.Read();
						List<object> headers = new List<object>();
						for(int i = 0; i < reader.FieldCount; i++)
						{
							headers.Add(reader.GetName(i));	
						}
						rows.Add(headers);

						while(reader.Read())
						{
							object[] outArr = new object[reader.FieldCount];
							reader.GetValues(outArr);
							rows.Add(outArr.ToList());
						}
					}
				}
			}
				return null;
		}
	}
}
