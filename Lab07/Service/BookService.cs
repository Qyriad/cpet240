﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qyriad.Lab07.Repository;
using Qyriad.Lab07.DataModel;

namespace Qyriad.Lab07.Service
{
	public class BookService
	{
		private List<BookViewModel> _bookViews;
		public ImmutableList<BookViewModel> BookViews { get => this._bookViews.ToImmutableList(); }

		public BookService(BookRepository repo)
		{
			foreach(Book b in repo.Books)
			{
				this._bookViews.Add(new BookViewModel(b));
			}
		}
	}

	public class BookViewModel
	{
		public string Id;
		public string Title;
		public string Type;
		public string PublisherId;
		public string Price;
		public string Advance;
		public string Royalty;
		public string YtdSales;
		public string Notes;
		public string PublishDate;

		public BookViewModel(Book book)
		{
			this.Id = book.TitleId;
			this.Title = book.Title;
			this.Type = book.Type;
			this.PublisherId = book.PublisherId;
			this.Price = String.Format("{0:C2}", book.Price);
			this.Advance = String.Format("{0:C2}", book.Advance);
			this.Royalty = String.Format("{0:C2}", book.Royalty);
			this.YtdSales = String.Format("{0:C2}", book.YTDSales);
			this.Notes = book.Notes;
			// Holy hell I hope that string interpolation formatting works
			this.PublishDate = $"{book.PublishDate.Year:d4}-{book.PublishDate.Month:d2}-{book.PublishDate.Day:d2} {book.PublishDate.Hour:d2}:{book.PublishDate.Minute:d2}:{book.PublishDate.Second:d2}.{book.PublishDate.Millisecond:d3}";
		}
	}
}
