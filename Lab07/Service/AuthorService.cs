﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Qyriad.Lab07.Repository;
using Qyriad.Lab07.DataModel;
using System.Collections.Immutable;

namespace Qyriad.Lab07.Service
{
	public class AuthorService
	{
		private List<AuthorViewModel> _authorViews;
		public ImmutableList<AuthorViewModel> AuthorViews { get => this._authorViews.ToImmutableList(); }

		public AuthorService(AuthorRepository repo)
		{
			foreach(Author a in repo.Authors)
			{
				this._authorViews.Add(new AuthorViewModel(a));
			}
		}
	}

	public class AuthorViewModel
	{
		public string Id { get; private set; }
		public string FirstName;
		public string LastName;
		public string FullName
		{
			get => this.FirstName + " " + this.LastName;
		}
		public string PhoneNumber;
		public string StreetAddress;
		public string City;
		public string State;
		public string ZipCode;
		public string Contracted;

		public AuthorViewModel(Author author)
		{
			this.Id = author.Id;
			this.FirstName = author.FirstName;
			this.LastName = author.LastName;
			this.PhoneNumber = author.PhoneNumber;
			this.StreetAddress = author.StreetAddress;
			this.City = author.City;
			this.State = author.State;
			this.ZipCode = author.ZipCode;
			this.Contracted = author.IsContracted ? "Contracted" : "Not Contracted";
		}
	}
}
