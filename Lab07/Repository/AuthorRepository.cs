﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qyriad.Lab07.DataModel;


namespace Qyriad.Lab07.Repository
{
	public class AuthorRepository : IRepository<Author>
	{
		public List<Author> Authors { get; private set; }
		public List<string> Fields { get; private set; }

		public AuthorRepository(List<List<object>> sqlAuthorRows, IEnumerable<string> sqlAuthorHeaders, List<List<object>> sqlTitleAuthorRows, IEnumerable<string> sqlTitleAuthorHeaders)
		{
			this.Authors = new List<Author>();
			this.Fields = new List<string>(sqlAuthorHeaders);

			foreach(List<object> row in sqlAuthorRows)
			{
				string id = row[0].ToString();
				string lname = row[1].ToString();
				string fname = row[2].ToString();
				string phone = row[3].ToString();
				string addr = row[4].ToString();
				string city = row[5].ToString();
				string state = row[6].ToString();
				string zip = row[7].ToString();
				bool contract = (bool)row[8];
				List<List<object>> titleRowsWithId = sqlTitleAuthorRows.FindAll(taRow => taRow[0].ToString() == id);
				List<string> tids = (from r in titleRowsWithId select r[0].ToString()).ToList();

				List<Author.Book_t> books = new List<Author.Book_t>(from r in titleRowsWithId select (new Author.Book_t(r[1].ToString(), (int)(byte)r[2])));

				Author a = new Author(id, lname, fname, phone, addr, city, state, zip, contract, books);
				this.Authors.Add(a);
			}
		}

		public List<Author> FindAll() => this.Authors;
	}
}
