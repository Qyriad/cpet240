﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lab07.Repository
{
	interface IRepository<T>
	{
		List<string> Fields { get; }
		List<T> FindAll();
	}
}
