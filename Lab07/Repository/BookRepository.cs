﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qyriad.Lab07.DataModel;

namespace Qyriad.Lab07.Repository
{
	public class BookRepository : IRepository<Book>
	{
		public List<Book> Books { get; private set; }
		public List<string> Fields { get; private set; }

		public BookRepository(List<List<object>> sqlRows, IEnumerable<string> cols, List<List<object>> sqlTitleAuthorRows, IEnumerable<string> sqlTitleAuthorHeaders)
		{
			this.Books = new List<Book>();
			this.Fields = new List<string>(cols);

			// Basically, perform .ToString() on every element in every element in sqlRows
			//foreach(List<string> row in from r in sqlRows select (from field in r where field is string select field.ToString()))
			// I made one of the most beautiful LINQ one liners and I can't even use it :'(
			foreach(List<object> row in sqlRows)
			{
				string id = row[0].ToString();
				string title = row[1].ToString();
				string type = row[2].ToString().Trim();
				string pubId = row[3].ToString();
				decimal? price = row[4] as decimal?;
				decimal? advance = row[5] as decimal?;
				int? royalty = row[6] as int?;
				int? ytdSales = row[7] as int?;
				string notes = row[8].ToString();

				// God I hope this works
				DateTime pubDate = ((DateTime)row[9]);

				List<List<object>> titleRowsWithId = sqlTitleAuthorRows.FindAll(taRow => taRow[1].ToString() == id);
				List<string> aids = (from r in titleRowsWithId select r[1].ToString()).ToList();
				List<Book.Author_t> authors = new List<Book.Author_t>(from r in titleRowsWithId select (new Book.Author_t(r[0].ToString(), (int)(byte)r[2])));

				Book book = new Book(id, title, type, pubId, price, advance, royalty, ytdSales, notes, pubDate, authors);
				this.Books.Add(book);
			}
		}

		public List<Book> FindAll() => this.Books;
	}
}
