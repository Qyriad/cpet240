﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lab07.DataAccess
{
	public class DataReader
	{
		/// <summary>
		/// Reads each row from an SQL table. First "row" returned is row "0", I.E. the column headers. 
		/// </summary>
		/// <param name="table">The name of the table to retrieve</param>
		/// <returns>A List of Lists of objects, each object is a field in the row. 
		/// The first "row" is row "0", I.E. the column headers. </returns>
		public static List<List<object>> ReadAllRowsFromTable(string table)
		{
			List<List<object>> rows = new List<List<object>>();
			using(SqlConnection connection = new SqlConnection(Const.CONNECTION_STRING))
			{
				connection.Open();
				connection.ChangeDatabase("pubs");
				using(SqlCommand cmd = new SqlCommand($"select * from {table}", connection))
				{
					using(SqlDataReader reader = cmd.ExecuteReader())
					{
						// First get the headers
						reader.Read();
						List<object> headers = new List<object>();
						for(int i = 0; i < reader.FieldCount; i++)
						{
							headers.Add(reader.GetName(i));
						}
						rows.Add(headers);

						// Now get the rest of the rows
						while(reader.Read())
						{
							object[] outarr = new object[reader.FieldCount];
							reader.GetValues(outarr);
							rows.Add(outarr.ToList());
						}
					}
				}
			}

			return rows;
		}
	}
}
