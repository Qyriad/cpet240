﻿namespace Qyriad.Lab07.GUI
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.svMain = new System.Windows.Forms.SplitContainer();
			this.tvMain = new System.Windows.Forms.TreeView();
			this.lvMain = new System.Windows.Forms.ListView();
			((System.ComponentModel.ISupportInitialize)(this.svMain)).BeginInit();
			this.svMain.Panel1.SuspendLayout();
			this.svMain.Panel2.SuspendLayout();
			this.svMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// svMain
			// 
			this.svMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.svMain.Location = new System.Drawing.Point(0, 0);
			this.svMain.Name = "svMain";
			// 
			// svMain.Panel1
			// 
			this.svMain.Panel1.Controls.Add(this.tvMain);
			// 
			// svMain.Panel2
			// 
			this.svMain.Panel2.Controls.Add(this.lvMain);
			this.svMain.Size = new System.Drawing.Size(652, 476);
			this.svMain.SplitterDistance = 418;
			this.svMain.TabIndex = 0;
			// 
			// tvMain
			// 
			this.tvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tvMain.Location = new System.Drawing.Point(3, 3);
			this.tvMain.Name = "tvMain";
			this.tvMain.Size = new System.Drawing.Size(412, 470);
			this.tvMain.TabIndex = 0;
			this.tvMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMain_AfterSelect);
			// 
			// lvMain
			// 
			this.lvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lvMain.Location = new System.Drawing.Point(3, 3);
			this.lvMain.Name = "lvMain";
			this.lvMain.Size = new System.Drawing.Size(224, 470);
			this.lvMain.TabIndex = 0;
			this.lvMain.UseCompatibleStateImageBehavior = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(652, 476);
			this.Controls.Add(this.svMain);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.svMain.Panel1.ResumeLayout(false);
			this.svMain.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.svMain)).EndInit();
			this.svMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer svMain;
		private System.Windows.Forms.TreeView tvMain;
		private System.Windows.Forms.ListView lvMain;
	}
}