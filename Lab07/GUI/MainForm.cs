﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Qyriad.Lab07.Repository;
using Qyriad.Lab07.DataModel;
using Qyriad.Lab07.Service;

namespace Qyriad.Lab07.GUI
{
	public partial class MainForm : Form
	{
		private AuthorRepository AuRepo;
		private BookRepository BookRepo;
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			lvMain.View = View.Details;
			List<List<object>> titleAuthorRows = DataAccess.DataReader.ReadAllRowsFromTable("titleauthor");

			List<List<object>> bookRows = DataAccess.DataReader.ReadAllRowsFromTable("titles");
			BookRepo = new BookRepository(bookRows.Skip(1).ToList(), from header in bookRows[0] where header is string select header as string, titleAuthorRows.Skip(1).ToList(), from header in titleAuthorRows[0] where header is string select header as string);

			List<List<object>> auRows = DataAccess.DataReader.ReadAllRowsFromTable("authors");
			// God I love LINQ													  // Basically just convert all the objects to strings
			AuRepo = new AuthorRepository(auRows.Skip(1).ToList(), from header in auRows[0] where header is string select header as string, titleAuthorRows.Skip(1).ToList(), from header in titleAuthorRows[0] where header is string select header as string);

			TreeNode auMainNode = tvMain.Nodes.Add("Authors");

			foreach(Author a in AuRepo.Authors)
			{
				AuthorViewModel avm = new AuthorViewModel(a);
				TreeNode aNode = auMainNode.Nodes.Add(avm.Id);
				aNode.Nodes.Add(avm.LastName);
				aNode.Nodes.Add(avm.FirstName);
				aNode.Nodes.Add(avm.StreetAddress);
				aNode.Nodes.Add(avm.City);
				aNode.Nodes.Add(avm.State);
				aNode.Nodes.Add(avm.ZipCode);
				aNode.Nodes.Add(avm.Contracted);
				TreeNode bNode = aNode.Nodes.Add("Books");
				foreach(Author.Book_t book in a.Books)
				{
					bNode.Nodes.Add(book.Id);
				}
			}

			TreeNode bookMainNode = tvMain.Nodes.Add("Books");

			foreach(Book book in BookRepo.Books)
			{
				BookViewModel bvm = new BookViewModel(book);
				TreeNode bNode = bookMainNode.Nodes.Add(bvm.Id);
				bNode.Nodes.Add(bvm.Title);
				bNode.Nodes.Add(bvm.Type);
				bNode.Nodes.Add(bvm.PublisherId);
				bNode.Nodes.Add(bvm.Price);
				bNode.Nodes.Add(bvm.Advance);
				bNode.Nodes.Add(bvm.Royalty);
				bNode.Nodes.Add(bvm.YtdSales);
				bNode.Nodes.Add(bvm.Notes);
				bNode.Nodes.Add(bvm.PublishDate);
				TreeNode auNode = bNode.Nodes.Add("Authors");
				foreach(Book.Author_t author in book.Authors)
				{
					auNode.Nodes.Add(author.Id);
				}	
			}
		}


		private void tvMain_AfterSelect(object sender, TreeViewEventArgs args)
		{
			lvMain.Clear();
			if(args.Node.Parent?.Text == "Authors" && args.Node.Parent?.Parent?.Parent?.Text == "Books")
			{
				string auId = args.Node.Text;
				List<Author> authors = AuRepo.Authors.FindAll(e => e.Id == auId);
				foreach(string f in AuRepo.Fields)
				{
					lvMain.Columns.Add(f);
				}
				foreach(Author a in authors)
				{
					lvMain.Items.Add(new ListViewItem(new string[] { a.Id, a.LastName, a.FirstName, a.PhoneNumber, a.StreetAddress, a.City, a.State, a.ZipCode, a.IsContracted ? "Contracted" : "Not Contracted" }));
				}
			}
			else if(args.Node.Parent?.Text == "Books" && args.Node.Parent?.Parent?.Parent?.Text == "Authors")
			{
				string bId = args.Node.Text;
				List<Book> books = BookRepo.Books.FindAll(e => e.TitleId == bId);
				foreach(string f in BookRepo.Fields)
				{
					lvMain.Columns.Add(f);
				}
				foreach(Book b in books)
				{
					lvMain.Items.Add(new ListViewItem(new string[] { b.TitleId, b.Title, b.Type, b.PublisherId, b.Price.ToString(), b.Advance.ToString(), b.Royalty.ToString(), b.YTDSales.ToString(), b.Notes, b.PublishDate.ToString()}));
				}
			}
		}
	}
}
