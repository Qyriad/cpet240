﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lab07.DataModel
{
	public class Book
	{
		public string TitleId { get; private set; }
		public string Title { get; private set; }
		public string Type { get; private set; }
		public string PublisherId { get; private set; }
		public decimal? Price { get; private set; }
		public decimal? Advance { get; private set; }
		public int? Royalty { get; private set; }
		public int? YTDSales { get; private set; }
		public string Notes { get; private set; }
		public DateTime PublishDate { get; private set; }
		public List<Author_t> Authors { get; private set; }

		public Book(string titleId, string title, string type, string pubId, decimal? price, decimal? advance, int? royalty, int? ytdSales, string notes, DateTime pubDate, List<Author_t> authors)
		{
			TitleId = titleId;
			Title = title;
			Type = type;
			PublisherId = pubId;
			Price = price;
			Advance = advance;
			Royalty = royalty;
			YTDSales = ytdSales;
			Notes = notes;
			PublishDate = pubDate;
			Authors = authors;
		}

		public class Author_t : Tuple<string, int>
		{
			public string Id { get; private set; }
			public int Position { get; private set; }

			public Author_t(string id, int position) : base(id, position)
			{
				Id = Item1;
				Position = Item2;
			}
		}
	}
}
