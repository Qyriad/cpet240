﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lab07.DataModel
{

	public class Author
	{
		public string Id { get; private set; }
		public string LastName { get; private set; }
		public string FirstName { get; private set; }
		public string PhoneNumber { get; private set; }
		public string StreetAddress { get; private set; }
		public string City { get; private set; }
		public string State { get; private set; }
		public string ZipCode { get; private set; }
		public bool IsContracted { get; private set; }
		public List<Book_t> Books { get; private set; }
		public class Book_t : Tuple<string, int>
		{
			public string Id { get; private set; }
			public int AuthorPosition { get; private set; }

			public Book_t(string id, int authorPosition) : base(id, authorPosition)
			{
				Id = Item1;
				AuthorPosition = Item2;
			}
		}

		public Author(string id, string lastName, string firstName, string phoneNumber, string streetAddress, string city,
			string state, string zipCode, bool isContracted, List<Book_t> books)
		{
			this.Id = id;
			this.LastName = lastName;
			this.FirstName = firstName;
			this.PhoneNumber = phoneNumber;
			this.StreetAddress = streetAddress;
			this.City = city;
			this.State = state;
			this.ZipCode = zipCode;
			this.IsContracted = isContracted;
			this.Books = books;
		}

		public string[] ToStringArray()
		{
			string[] s = new string[9];
			s[0] = this.Id;
			s[1] = this.LastName;
			s[2] = this.FirstName;
			s[3] = this.PhoneNumber;
			s[4] = this.StreetAddress;
			s[5] = this.City;
			s[6] = this.State;
			s[7] = this.ZipCode;
			s[8] = this.IsContracted.ToString();

			return s;
		}
	}
}
