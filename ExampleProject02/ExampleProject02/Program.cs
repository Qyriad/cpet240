﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject02
{
	class person
	{
		private string _name;
		public int age { get; private set; }
		public person(string name, int age)
		{
			_name = name;
			this.age = age;
		}

		public string name
		{
			get { return _name; }
			set { _name = value; }
		}

		public void birthday()
		{
			age++;
		}

		public override string ToString()
		{
			return _name +  ", " + age.ToString();
		}
	}
	class Program
	{
		static void f(person p)
		{
			p.name = "Jane";
			p.birthday();
		}
		static void Main(string[] args)
		{
			List<person> people = new List<person>(); // List is a generic collection
			people.Add(new person("Mikaela", 19));
			people.Add(new person("Ken", 54));
			Console.WriteLine(typeof(int).BaseType.BaseType.BaseType.FullName);

			foreach(person p in people) Console.WriteLine(p);
		}
	}
}
