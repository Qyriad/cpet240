// File:	main.cpp
// Project:	CPET240/FinalExam
// Author:	Mikaela RJ Szekely
// Date: 12/13/2017

#include <iostream>
#include <conio.h>

#include <Windows.h> 

VOID CALLBACK MsgBoxCallback(LPHELPINFO lpHelpInfo);

int main()
{
	MSGBOXPARAMS MsgBoxParams = { 0 };

	MsgBoxParams.cbSize = sizeof(MsgBoxParams);
	MsgBoxParams.lpszText = "This is a message";
	MsgBoxParams.lpszCaption = "Caption";
	MsgBoxParams.dwStyle = MB_YESNOCANCEL | MB_ICONQUESTION | MB_SYSTEMMODAL;
	MsgBoxParams.lpfnMsgBoxCallback = &MsgBoxCallback;
	MsgBoxParams.dwContextHelpId = 666;

	int ret = MessageBoxIndirect(&MsgBoxParams);

	switch(ret)
	{
		case IDYES:
			std::cout << "Yes" << std::endl;
			break;
		case IDNO:
			std::cout << "No" << std::endl;
			break;
		case IDCANCEL:
			std::cout << "Cancel" << std::endl;
			break;
	}

	_getch();

	return 0;
}


VOID CALLBACK MsgBoxCallback(LPHELPINFO lpHelpInfo)
{
	std::cout << "Help!" << std::endl;
	POINT pos = lpHelpInfo->MousePos;
	std::cout << "Mouse Position: (" << pos.x << ", " << pos.y << ")" << std::endl;
	std::cout << "Context ID: " << lpHelpInfo->dwContextId << std::endl;
}